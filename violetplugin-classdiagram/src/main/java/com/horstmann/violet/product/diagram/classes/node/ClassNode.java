package com.horstmann.violet.product.diagram.classes.node;

import java.awt.Color;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.horstmann.violet.framework.graphics.Separator;
import com.horstmann.violet.framework.graphics.content.*;
import java.awt.*;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.util.Arrays;
import java.util.List;

import com.horstmann.violet.framework.graphics.Separator;
import com.horstmann.violet.framework.graphics.content.*;
import com.horstmann.violet.framework.graphics.shape.ContentInsideCustomShape;
import com.horstmann.violet.framework.graphics.shape.ContentInsideRectangle;
import com.horstmann.violet.product.diagram.abstracts.edge.IEdge;
import com.horstmann.violet.product.diagram.abstracts.node.AbstractNode;
import com.horstmann.violet.product.diagram.abstracts.node.INode;
import com.horstmann.violet.product.diagram.classes.ClassDiagramConstant;
import com.horstmann.violet.product.diagram.property.choiceList.ChoiceList;
import com.horstmann.violet.product.diagram.property.choiceList.TextChoiceList;
import com.horstmann.violet.product.diagram.common.edge.NoteEdge;
import com.horstmann.violet.product.diagram.common.node.NoteNode;
import com.horstmann.violet.product.diagram.property.text.LineText;
import com.horstmann.violet.product.diagram.property.text.MultiLineText;
import com.horstmann.violet.product.diagram.property.text.SingleLineText;
import com.horstmann.violet.product.diagram.property.text.decorator.*;

import static java.lang.Character.toUpperCase;

/**
 * A class node in a class diagram.
 */
public class ClassNode extends AbstractNode
{
    /**
     * Construct a class node with a default size
     */

    public static boolean changeSizeLetter = false;

    public ClassNode()
    {
        super();
        type = new TextChoiceList<String>(TYPE_KEYS, TYPE_VALUE);
        selectedType = type.getSelectedPos();
        title = new SingleLineText();
        title.setAlignment(LineText.CENTER);
        title.setPadding(5, 10, 0, 10);
        name = new SingleLineText(NAME_CONVERTER);
        name.setAlignment(LineText.CENTER);
        name.setPadding(0, 10, 5, 10);
        attributes = new MultiLineText(PROPERTY_CONVERTER);
        methods = new MultiLineText(PROPERTY_CONVERTER);
        createContentStructure();
    }


    protected ClassNode(ClassNode node) throws CloneNotSupportedException
    {
        super(node);
        type = node.type;
        selectedType = type.getSelectedPos();
        title = node.title.clone();
        title.setPadding(5, 10, 0, 10);
        name = node.name.clone();
        name.setPadding(0, 10, 5, 10);
        attributes = node.attributes.clone();
        methods = node.methods.clone();
        createContentStructure();
    }

    public boolean changingLetter(boolean changeSizeLetter){

        this.changeSizeLetter = changeSizeLetter;

        return changeSizeLetter;
    }

    @Override
    protected void beforeReconstruction()
    {
        super.beforeReconstruction();
        if (null == title)
        {
            title = new SingleLineText();
        }
        if (null == name)
        {
            name = new SingleLineText();
        }
        if (null == attributes)
        {
            attributes = new MultiLineText();
        }
        if (null == methods)
        {
            methods = new MultiLineText();
        }

        type = new TextChoiceList<String>(TYPE_KEYS, TYPE_VALUE);
        type.setSelectedIndex(selectedType);
        title.reconstruction();
        title.setAlignment(LineText.CENTER);
        title.setPadding(5, 10, 0, 10);
        name.reconstruction(NAME_CONVERTER);
        name.setAlignment(LineText.CENTER);
        name.setPadding(0, 10, 5, 10);
        attributes.reconstruction(PROPERTY_CONVERTER);
        methods.reconstruction(PROPERTY_CONVERTER);
    }



    @Override
    protected INode copy() throws CloneNotSupportedException
    {
        return new ClassNode(this);
    }


    @Override
    public void setNodeState(INode nodeState) {
        ClassNode node = (ClassNode) nodeState;
        title.setText(node.title);
        name.setText(node.name);
        attributes.setText(node.attributes);
        methods.setText(node.methods);
        createContentStructure();
    }

    @Override
    protected void createContentStructure()
    {
        TextContent titleContent = new TextContent(title);


        TextContent nameContent = new TextContent(name);
        nameContent.setMinHeight(MIN_NAME_HEIGHT);
        nameContent.setMinWidth(MIN_WIDTH);
        TextContent attributesContent = new TextContent(attributes);
        TextContent methodsContent = new TextContent(methods);

        VerticalLayout verticalInsideGroupContent = new VerticalLayout();
        verticalInsideGroupContent.add(new CenterContent(titleContent));
        verticalInsideGroupContent.add(new CenterContent(nameContent));

        VerticalLayout verticalGroupContent = new VerticalLayout();
        verticalGroupContent.add(verticalInsideGroupContent);
        verticalGroupContent.add(attributesContent);
        verticalGroupContent.add(methodsContent);

        separator = new Separator.LineSeparator(getBorderColor());
        verticalGroupContent.setSeparator(separator);
        ContentInsideShape contentInsideShape = new ContentInsideRectangle(verticalGroupContent);

        setBorder(new ContentBorder(contentInsideShape, getBorderColor()));
        setBackground(new ContentBackground(getBorder(), getBackgroundColor()));
        setContent(getBackground());

        setTextColor(super.getTextColor());
    }

    @Override
    public void setBorderColor(Color borderColor)
    {
        if (null != separator)
        {
            separator.setColor(borderColor);
        }
        super.setBorderColor(borderColor);
    }

    @Override
    public void setTextColor(Color textColor)
    {
        title.setTextColor(textColor);
        name.setTextColor(textColor);
        attributes.setTextColor(textColor);
        methods.setTextColor(textColor);
        super.setTextColor(textColor);
    }


    @Override
    public String getToolTip()
    {
        return ClassDiagramConstant.CLASS_DIAGRAM_RESOURCE.getString("tooltip.class_node");
    }

    /**
     * Sets the title property value.
     *
     * @param newValue the class title
     */
    public void setTitle( LineText newValue)
    {
        title.setText(newValue.toEdit());
    }

    /**
     * Gets the title property value.
     *
     * @return the class title
     */
    public SingleLineText getTitle()
    {
        return title;
    }

    /**
     * Sets the name property value.
     *
     * @param newValue the class name
     */
    public void setName(LineText newValue)
    {
        name.setText(newValue);
    }

    /**
     * Gets the name property value.
     *
     * @return the class name
     */
    public LineText getName()
    {
        return name;
    }

    /**
     * Sets the attributes property value.
     *
     * @param newValue the attributes of this class
     */
    public void setAttributes(LineText newValue)
    {
        attributes.setText(newValue);
    }

    /**
     * Gets the attributes property value.
     *
     * @return the attributes of this class
     */
    public LineText getAttributes()
    {
        return attributes;
    }

    /**
     * Sets the methods property value.
     *
     * @param newValue the methods of this class
     */
    public void setMethods(LineText newValue)
    {
        methods.setText(newValue);
    }

    /**
     * Gets the methods property value.
     *
     * @return the methods of this class
     */
    public LineText getMethods()
    {
        return methods;
    }

    /**
     * Gets the type property value.
     *
     * @return the type of this class
     */
    public ChoiceList getType()
    {
        return type;
    }

    /**
     * Sets the  type property value.
     *
     * @param type the class name
     */
    public void setType(ChoiceList type)
    {
        this.type = (TextChoiceList<String>) type;
        selectedType = this.type.getSelectedPos();
        updateTitle((String) type.getSelectedValue());

    }

    private void updateTitle(String header)
    {
        if (header.equals("default"))
        {
            title.setText("");
        }
        else
        {

            title.setText("«" + header + "»");
        }
    }

    private SingleLineText name;
    private SingleLineText title;
    private MultiLineText attributes;
    private MultiLineText methods;

    private int selectedType;
    private transient TextChoiceList<String> type;

    private transient Separator separator;

    private static final int MIN_NAME_HEIGHT = 45;
    private static final int MIN_WIDTH = 100;
    private static final String STATIC = "<<static>>";
    //private static final String ABSTRACT = "«abstract»";
    private static final String[][] SIGNATURE_REPLACE_KEYS = {
            { "public ", "+ " },
            { "package ", "~ " },
            { "protected ", "# " },
            { "private ", "- " },
            { "property ", "/ " }
    };

    private static final String[] TYPE_VALUE = new String[] {
            "default",
            "utility",
            "type",
            "metaclass",
            "implementation",
            "focus",
            "entity",
            "control",
            "boundary",
            "auxiliary",
            "abstract",
            "static"
    };

    private static final LineText.Converter NAME_CONVERTER = new LineText.Converter()
    {
        @Override
        public OneLineText toLineString(String text)
        {
            OneLineText controlText;
            OneLineText lineString;
            String largerFirstLetter="";

            if(changeSizeLetter == true){
                if(text != null && text.length() > 0) {
                    largerFirstLetter = toUpperCase(text.charAt(0)) + text.substring(1);
                }

                controlText = new OneLineText(largerFirstLetter);
                lineString = new LargeSizeDecorator(controlText);

            } else {

                controlText = new OneLineText(text);
                lineString = new LargeSizeDecorator(controlText);
            }

            if (controlText.toString().matches("(((<<[^<>]*>>)*\\s*)*)\\V*"))
            {
                List<String> allMatches = new ArrayList<String>();
                checkForPattern(controlText, allMatches);
                Collections.reverse(allMatches);

                for (String s : allMatches)
                {
                    lineString = new PrefixDecorator(new RemoveSentenceDecorator(
                            lineString, s), String.format("<center>%s</center>", s));
                }

            }

            return lineString;
        }

        private List<String> checkForPattern(OneLineText controlText, List<String> allMatches)
        {
            Matcher m = Pattern.compile("(<<[^<>]*>>)").matcher(controlText.toString());
            while (m.find())
            {
                String temp = m.group().replace(">>", "»").replace("<<", "«");
                if (!allMatches.contains(temp))
                {
                    allMatches.add(temp);
                }
            }
            return allMatches;
        }



    };
    protected static final String[] TYPE_KEYS = new String[TYPE_VALUE.length];

    static
    {
        for (int i = 0; i < TYPE_KEYS.length; ++i)
        {
            try
            {
                TYPE_KEYS[i] = ClassDiagramConstant.CLASS_DIAGRAM_RESOURCE.getString(
                        ("type." + TYPE_VALUE[i]).toLowerCase()
                );
            }
            catch (MissingResourceException e)
            {
                //ignore
            }
        }
    }

    private static final LineText.Converter PROPERTY_CONVERTER = new LineText.Converter()
    {
        @Override
        public OneLineText toLineString(String text)
        {
            OneLineText lineString = new OneLineText(text);

            if (lineString.contains(STATIC))
            {
                lineString = new UnderlineDecorator(new RemoveSentenceDecorator(lineString, STATIC));
            }
            for (String[] signature : SIGNATURE_REPLACE_KEYS)
            {
                lineString = new ReplaceSentenceDecorator(lineString, signature[0], signature[1]);
            }

            return lineString;
        }
    };
}
