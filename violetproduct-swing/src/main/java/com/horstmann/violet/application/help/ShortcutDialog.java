package com.horstmann.violet.application.help;

import com.horstmann.violet.application.gui.MainFrame;
import com.horstmann.violet.framework.injection.resources.ResourceBundleInjector;
import com.horstmann.violet.framework.injection.resources.ResourceShortcutProvider;
import com.horstmann.violet.framework.injection.resources.annotation.ResourceBundleBean;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;


/**
 * Class responsible for create shortcut dialog
 * Created by piter on 02.01.16.
 */
public class ShortcutDialog extends JDialog {
    @ResourceBundleBean(key = "dialog.title")
    private String dialogTitle;

    @ResourceBundleBean(key = "dialog.table.behavior")
    private String behaviorName;

    @ResourceBundleBean(key = "dialog.table.shortcut")
    private String shortcut;

    @ResourceBundleBean(key = "dialog.table.nodata")
    private String noData;

    @ResourceBundleBean(key = "dialog.table.shortcutEditInfo")
    private String shortcutEditInfo;

    private JPanel shortcutPanel;
    private JFrame jFrame;

    /**
     * Default constructor of ShortcutDialog
     *
     * @param parent JFrame parent
     */
    public ShortcutDialog(JFrame parent) {
        super(parent);
        ResourceBundleInjector.getInjector().inject(this);

        this.jFrame = parent;
        this.setTitle(dialogTitle);
        this.setLocationRelativeTo(null);
        this.setModal(true);
        this.setResizable(false);
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.getContentPane().setLayout(new BorderLayout());
        this.getContentPane().add(buildShortcutPanel(), BorderLayout.CENTER);
        this.getContentPane().add(buildShortcutInfoTextField(), BorderLayout.SOUTH);
        this.pack();
        this.setCenterLocation(parent);
    }

    /**
     * Creates panel with shortcuts
     *
     * @return shortcut panel
     */
    public JPanel buildShortcutPanel() {
        if (shortcutPanel == null) {
            shortcutPanel = new JPanel(new BorderLayout());

            String[] columnNames = {behaviorName, shortcut};

            JTable table = new JTable(prepareDataForTable(), columnNames);
            table.setEnabled(false);
            table.setCellSelectionEnabled(false);
            table.setShowGrid(true);
            table.setGridColor(new Color(220, 220, 220));
            table.setRowHeight(30);
            table.setIntercellSpacing(new Dimension(15, 0));
            table.getTableHeader().setFont(new Font("Times New Roman", Font.BOLD, 14));

            final ShortcutDialog shortcutFrame = this;
            table.addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent mouseEvent) {
                    final JTable sourceTable = (JTable) mouseEvent.getSource();
                    final Point point = mouseEvent.getPoint();
                    final int row = sourceTable.rowAtPoint(point);
                    if (mouseEvent.getClickCount() == 2 && row != -1) {
                        final String behavior = sourceTable.getValueAt(row, 0).toString();
                        final String accelerator = sourceTable.getValueAt(row, 1).toString();

                        final NewShortcutDialog newShortcutDialog =
                                new NewShortcutDialog(shortcutFrame, behavior, accelerator);
                        newShortcutDialog.setVisible(true);
                    }
                }
            });

            JScrollPane scrollPane = new JScrollPane(table);
            shortcutPanel.add(scrollPane, BorderLayout.CENTER);

            this.closeByEscHandler();
        }
        return this.shortcutPanel;
    }

    private String[][] prepareDataForTable() {
        final HashMap<String, String> allShortcuts = ResourceShortcutProvider.getInstance().getAllShortcuts();
        final int shortcutNumber = allShortcuts.size();
        String[][] shortcutArray;

        if (shortcutNumber != 0) {
            shortcutArray = new String[shortcutNumber][2];

            int counter = 0;
            for (Map.Entry<String, String> entry : ResourceShortcutProvider.getInstance().getAllShortcuts().entrySet()) {
                shortcutArray[counter][0] = entry.getKey();
                shortcutArray[counter][1] = entry.getValue();
                counter++;
            }
        } else {
            shortcutArray = new String[1][2];
            shortcutArray[0][0] = noData;
            shortcutArray[0][1] = noData;
        }
        return shortcutArray;
    }

    private void setCenterLocation(JFrame parent) {
        setLocation((parent.getWidth() - getWidth()) / 2, (parent.getHeight() - getHeight()) / 2);
    }

    private JTextField buildShortcutInfoTextField() {
        final JTextField textField = new JTextField();
        textField.setEditable(false);
        textField.setText(shortcutEditInfo);
        return textField;
    }

    public MainFrame getMainFrame() {
        if (this.jFrame instanceof MainFrame) {
            return (MainFrame) this.jFrame;
        }
        return null;
    }

    public void closeByEscHandler() {
        this.getRootPane().registerKeyboardAction(e -> {
            this.dispose();
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
    }

    void setShortcutPanel(JPanel shortcutPanel) {
        this.shortcutPanel = shortcutPanel;
    }

    JPanel getShortcutPanel() {
        return shortcutPanel;
    }
}