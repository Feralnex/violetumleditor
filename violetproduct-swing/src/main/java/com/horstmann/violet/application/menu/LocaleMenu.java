package com.horstmann.violet.application.menu;

import com.horstmann.violet.application.gui.MainFrame;
import com.horstmann.violet.framework.dialog.DialogFactory;
import com.horstmann.violet.framework.injection.bean.ManiocFramework.BeanInjector;
import com.horstmann.violet.framework.injection.bean.ManiocFramework.InjectedBean;
import com.horstmann.violet.framework.injection.resources.ResourceBundleInjector;
import com.horstmann.violet.framework.injection.resources.annotation.ResourceBundleBean;
import com.horstmann.violet.framework.locale.LocaleManager;

import javax.swing.*;
import java.util.List;

/**
 * Used to set language
 */
@ResourceBundleBean(resourceReference = MenuFactory.class)
public class LocaleMenu extends JMenu
{
    private JFrame mainFrame;

    @InjectedBean
    private DialogFactory dialogFactory;

    @ResourceBundleBean(key = "locale.change.title")
    private String localeChangeTitle;

    @ResourceBundleBean(key = "locale.change.message")
    private String localeChangeMessage;

    @ResourceBundleBean(key = "locale.change.icon")
    private ImageIcon localeChangeIcon;

    @ResourceBundleBean(key = "locale")
    public LocaleMenu(final MainFrame mainFrame)
    {
        BeanInjector.getInjector().inject(this);
        ResourceBundleInjector.getInjector().inject(this);
        this.mainFrame = mainFrame;
        this.createMenu();
    }

    private void createMenu()
    {
        final List<String> localeCodes = LocaleManager.getSupportedLocaleCodes();
        final String prefLocaleCode = LocaleManager.getCurrentLocaleCode();
        final ButtonGroup languageButtonGroup = new ButtonGroup();
        for (final String localeCode : localeCodes)
        {
            JMenuItem localeItem = new JCheckBoxMenuItem(localeCode);
            localeItem.addActionListener(actionEvent -> {
                selectLocale(localeCode);
            });
            if (localeCode.equals(prefLocaleCode))
            {
                localeItem.setSelected(true);
            }
            this.add(localeItem);
            languageButtonGroup.add(localeItem);
        }
    }

    private void selectLocale(final String localeCode)
    {
        LocaleManager.setLocale(localeCode);
        final JOptionPane optionPane = new JOptionPane();
        optionPane.setMessage(localeChangeMessage);
        optionPane.setIcon(localeChangeIcon);
        this.dialogFactory.showDialog(optionPane, localeChangeTitle, true);
    }
}
