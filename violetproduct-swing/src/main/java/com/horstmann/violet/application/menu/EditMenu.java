/*
 Violet - A program for editing UML diagrams.

 Copyright (C) 2007 Cay S. Horstmann (http://horstmann.com)
 Alexandre de Pellegrin (http://alexdp.free.fr);

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package com.horstmann.violet.application.menu;

import com.horstmann.violet.application.gui.MainFrame;
import com.horstmann.violet.application.gui.MarginDialog;
import com.horstmann.violet.framework.injection.resources.ResourceBundleInjector;
import com.horstmann.violet.framework.injection.resources.annotation.ResourceBundleBean;
import com.horstmann.violet.workspace.editorpart.IEditorPart;
import com.horstmann.violet.workspace.editorpart.IEditorPartBehaviorManager;
import com.horstmann.violet.workspace.editorpart.behavior.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * Edit menu
 *
 * @author Alexandre de Pellegrin
 *
 */
@ResourceBundleBean(resourceReference = MenuFactory.class)
public class EditMenu extends JMenu
{

    /**
     * Default constructor
     *
     * @param mainFrame where is attached this menu
     */
    @ResourceBundleBean(key = "edit")
    public EditMenu(final MainFrame mainFrame)
    {
        ResourceBundleInjector.getInjector().inject(this);
        this.mainFrame = mainFrame;
        this.createMenu();
    }

    /**
     * Initializes menu
     */
    private void createMenu()
    {
        undo.addActionListener(this::undoActionPerformed);
        this.add(undo);

        redo.addActionListener(this::redoActionPerformed);
        this.add(redo);

        properties.addActionListener(this::addActionPerformed);
        this.add(properties);

        cut.addActionListener(this::cutActionPerformed);
        this.add(cut);

        copy.addActionListener(this::copyActionPerformed);
        this.add(copy);

        paste.addActionListener((ActionEvent event) -> {
            if (isThereAnyWorkspaceDisplayed()) {
                IEditorPart activeEditorPart = getActiveEditorPart();
                IEditorPartBehaviorManager behaviorManager = activeEditorPart.getBehaviorManager();
                List<CutCopyPasteBehavior> found = behaviorManager.getBehaviors(CutCopyPasteBehavior.class);
                if (found.size() != 1) {
                    return;
                }
                found.get(0).paste();
            }
        });
        this.add(paste);

        delete.addActionListener((ActionEvent event) -> {
            if (isThereAnyWorkspaceDisplayed()) {
                getActiveEditorPart().removeSelected();
            }
        });
        this.add(delete);

        selectAll.addActionListener((ActionEvent event) -> {
            if (isThereAnyWorkspaceDisplayed()) {
                IEditorPart activeEditorPart = getActiveEditorPart();
                IEditorPartBehaviorManager behaviorManager = activeEditorPart.getBehaviorManager();
                List<SelectAllBehavior> found = behaviorManager.getBehaviors(SelectAllBehavior.class);
                if (found.size() != 1) {
                    return;
                }
                found.get(0).selectAllGraphElements();
            }
        });
        select.add(selectAll);

        selectNext.addActionListener((ActionEvent event) -> {
            if (isThereAnyWorkspaceDisplayed()) {
                IEditorPart activeEditorPart = getActiveEditorPart();
                IEditorPartBehaviorManager behaviorManager = activeEditorPart.getBehaviorManager();
                List<SelectByDistanceBehavior> found = behaviorManager.getBehaviors(SelectByDistanceBehavior.class);
                if (found.size() != 1) {
                    return;
                }
                found.get(0).selectAnotherGraphElement(1);
            }
        });
        select.add(selectNext);

        selectPrevious.addActionListener((ActionEvent event) -> {
            if (isThereAnyWorkspaceDisplayed()) {
                IEditorPart activeEditorPart = getActiveEditorPart();
                IEditorPartBehaviorManager behaviorManager = activeEditorPart.getBehaviorManager();
                List<SelectByDistanceBehavior> found = behaviorManager.getBehaviors(SelectByDistanceBehavior.class);
                if (found.size() != 1) {
                    return;
                }
                found.get(0).selectAnotherGraphElement(-1);
            }
        });
        select.add(selectPrevious);

        invert.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent event)
            {
                if (isThereAnyWorkspaceDisplayed()) {
                    IEditorPart activeEditorPart = getActiveEditorPart();
                    IEditorPartBehaviorManager behaviorManager = activeEditorPart.getBehaviorManager();
                    List<InvertBehavior> found = behaviorManager.getBehaviors(InvertBehavior.class);
                    if (found.size() != 1) {
                        return;
                    }
                    found.get(0).invertArrowheads();
                }

            }
        });

        this.add(invert);

        properties.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent event)
            {
                if (isThereAnyWorkspaceDisplayed()) {
                    IEditorPart activeEditorPart = getActiveEditorPart();
                    IEditorPartBehaviorManager behaviorManager = activeEditorPart.getBehaviorManager();
                    List<EditSelectedBehavior> found = behaviorManager.getBehaviors(EditSelectedBehavior.class);
                    if (found.size() != 1) {
                        return;
                    }
                    found.get(0).editSelected();
                }
            }
        });

        splitIntoTwo.addActionListener(this::performSplitIntoTwo);
        this.add(splitIntoTwo);

        this.add(select);

        moveUp.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent event)
            {
                if (isThereAnyWorkspaceDisplayed()) {
                    IEditorPart activeEditorPart = getActiveEditorPart();
                    IEditorPartBehaviorManager behaviorManager = activeEditorPart.getBehaviorManager();
                    List<MoveSelectedByArrowKeyBehavior> found = behaviorManager.getBehaviors(MoveSelectedByArrowKeyBehavior.class);
                    if (found.size() != 1) {
                        return;
                    }
                    found.get(0).moveUp();
                }

            }
        });
        move.add(moveUp);

        moveDown.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent event)
            {
                if (isThereAnyWorkspaceDisplayed()) {
                    IEditorPart activeEditorPart = getActiveEditorPart();
                    IEditorPartBehaviorManager behaviorManager = activeEditorPart.getBehaviorManager();
                    List<MoveSelectedByArrowKeyBehavior> found = behaviorManager.getBehaviors(MoveSelectedByArrowKeyBehavior.class);
                    if (found.size() != 1) {
                        return;
                    }
                    found.get(0).moveDown();
                }

            }
        });
        move.add(moveDown);

        moveLeft.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent event)
            {
                if (isThereAnyWorkspaceDisplayed()) {
                    IEditorPart activeEditorPart = getActiveEditorPart();
                    IEditorPartBehaviorManager behaviorManager = activeEditorPart.getBehaviorManager();
                    List<MoveSelectedByArrowKeyBehavior> found = behaviorManager.getBehaviors(MoveSelectedByArrowKeyBehavior.class);
                    if (found.size() != 1) {
                        return;
                    }
                    found.get(0).moveLeft();
                }

            }
        });
        move.add(moveLeft);

        moveRight.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent event)
            {
                if (isThereAnyWorkspaceDisplayed()) {
                    IEditorPart activeEditorPart = getActiveEditorPart();
                    IEditorPartBehaviorManager behaviorManager = activeEditorPart.getBehaviorManager();
                    List<MoveSelectedByArrowKeyBehavior> found = behaviorManager.getBehaviors(MoveSelectedByArrowKeyBehavior.class);
                    if (found.size() != 1) {
                        return;
                    }
                    found.get(0).moveRight();
                }

            }
        });
        move.add(moveRight);

        chooseMargins.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                MarginDialog dialog = new MarginDialog(mainFrame);
                dialog.setVisible(true);
            }
        });
        this.add(chooseMargins);

        this.add(move);
    }

    private void performSplitIntoTwo(ActionEvent event) {
        if (isThereAnyWorkspaceDisplayed()) {
            IEditorPart activeEditorPart = getActiveEditorPart();
            IEditorPartBehaviorManager behaviorManager = activeEditorPart.getBehaviorManager();

            List<CutCopyPasteBehavior> found = behaviorManager.getBehaviors(CutCopyPasteBehavior.class);
            if (found.size() != 1) {
                return;
            }
            found.get(0).copy();
            found.get(0).paste();
        }

    }

    /**
     * @return current editor
     */
    private IEditorPart getActiveEditorPart()
    {
        return this.mainFrame.getActiveWorkspace().getEditorPart();
    }

    /**
     * @return true id at least one workspace is reachable
     */
    private boolean isThereAnyWorkspaceDisplayed()
    {
        return mainFrame.getWorkspaceList().size() > 0;
    }

    private void undoActionPerformed(ActionEvent event) {
        if (isThereAnyWorkspaceDisplayed()) {
            IEditorPart activeEditorPart = getActiveEditorPart();
            IEditorPartBehaviorManager behaviorManager = activeEditorPart.getBehaviorManager();
            List<UndoRedoCompoundBehavior> found = behaviorManager.getBehaviors(UndoRedoCompoundBehavior.class);
            if (found.size() != 1) {
                return;
            }
            found.get(0).undo();
        }
    }

    private void addActionPerformed(ActionEvent event) {
        if (isThereAnyWorkspaceDisplayed()) {
            IEditorPart activeEditorPart = getActiveEditorPart();
            IEditorPartBehaviorManager behaviorManager = activeEditorPart.getBehaviorManager();
            List<EditSelectedBehavior> found = behaviorManager.getBehaviors(EditSelectedBehavior.class);
            if (found.size() != 1) {
                return;
            }
            found.get(0).editSelected();
        }
    }

    private void redoActionPerformed(ActionEvent event) {
        if (isThereAnyWorkspaceDisplayed()) {
            IEditorPart activeEditorPart = getActiveEditorPart();
            IEditorPartBehaviorManager behaviorManager = activeEditorPart.getBehaviorManager();
            List<UndoRedoCompoundBehavior> found = behaviorManager.getBehaviors(UndoRedoCompoundBehavior.class);
            if (found.size() != 1) {
                return;
            }
            found.get(0).redo();
        }
    }

    private void cutActionPerformed(ActionEvent event) {
        if (isThereAnyWorkspaceDisplayed()) {
            IEditorPart activeEditorPart = getActiveEditorPart();
            IEditorPartBehaviorManager behaviorManager = activeEditorPart.getBehaviorManager();
            List<CutCopyPasteBehavior> found = behaviorManager.getBehaviors(CutCopyPasteBehavior.class);
            if (found.size() != 1) {
                return;
            }
            found.get(0).cut();
        }
    }

    private void copyActionPerformed(ActionEvent event) {
        if (isThereAnyWorkspaceDisplayed()) {
            IEditorPart activeEditorPart = getActiveEditorPart();
            IEditorPartBehaviorManager behaviorManager = activeEditorPart.getBehaviorManager();
            List<CutCopyPasteBehavior> found = behaviorManager.getBehaviors(CutCopyPasteBehavior.class);
            if (found.size() != 1) {
                return;
            }
            found.get(0).copy();
        }
    }

    /** Application frame */
    private MainFrame mainFrame;


    @ResourceBundleBean(key = "edit.undo")
    private JMenuItem undo;

    @ResourceBundleBean(key = "edit.redo")
    private JMenuItem redo;

    @ResourceBundleBean(key = "edit.invert")
    private JMenuItem invert;

    @ResourceBundleBean(key = "edit.properties")
    private JMenuItem properties;

    @ResourceBundleBean(key = "edit.cut")
    private JMenuItem cut;

    @ResourceBundleBean(key = "edit.copy")
    private JMenuItem copy;

    @ResourceBundleBean(key = "edit.paste")
    private JMenuItem paste;

    @ResourceBundleBean(key = "edit.delete")
    private JMenuItem delete;

    @ResourceBundleBean(key = "edit.select_all")
    private JMenuItem selectAll;

    @ResourceBundleBean(key = "edit.select_next")
    private JMenuItem selectNext;

    @ResourceBundleBean(key = "edit.select_previous")
    private JMenuItem selectPrevious;

    @ResourceBundleBean(key = "edit.margin")
    private JMenuItem chooseMargins;

    @ResourceBundleBean(key = "edit.split_into_two")
    private JMenuItem splitIntoTwo;

    @ResourceBundleBean(key = "edit.move_up")
    private JMenuItem moveUp;

    @ResourceBundleBean(key = "edit.move_down")
    private JMenuItem moveDown;

    @ResourceBundleBean(key = "edit.move_left")
    private JMenuItem moveLeft;

    @ResourceBundleBean(key = "edit.move_right")
    private JMenuItem moveRight;

    @ResourceBundleBean(key = "edit.move")
    private JMenu move;

    @ResourceBundleBean(key = "edit.select")
    private JMenu select;
}
