package com.horstmann.violet.application.help;

import com.horstmann.violet.application.gui.MainFrame;
import com.horstmann.violet.framework.injection.bean.ManiocFramework;
import com.horstmann.violet.framework.injection.bean.ManiocFramework.InjectedBean;
import com.horstmann.violet.framework.injection.resources.ResourceBundleInjector;
import com.horstmann.violet.framework.injection.resources.ResourceShortcutProvider;
import com.horstmann.violet.framework.injection.resources.annotation.ResourceBundleBean;
import com.horstmann.violet.framework.userpreferences.UserPreferencesService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * Class representing new shortcut dialog
 */
public class NewShortcutDialog extends JDialog implements KeyListener
{
    @ResourceBundleBean(key = "dialog.newShortcutInfo")
    private String newShortcutInfo;

    @ResourceBundleBean(key = "dialog.shortcutTakenAlert")
    private String shortcutTakenAlert;

    @InjectedBean
    private UserPreferencesService userPreferencesService;

    private ShortcutDialog parentFrame;
    private MainFrame mainFrame;
    private String behaviorName;
    private KeyStroke keyStroke;
    private JTextField alreadyTaken;

    public NewShortcutDialog(final ShortcutDialog parent, final String behaviorName, final String accelerator)
    {
        super(parent);
        ManiocFramework.BeanInjector.getInjector().inject(this);
        ResourceBundleInjector.getInjector().inject(this);

        this.setLocationRelativeTo(null);
        this.setModal(true);
        this.setResizable(false);
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.getContentPane().setLayout(new BorderLayout());
        this.getContentPane().add(askForShortcutDialog(), BorderLayout.CENTER);
        this.pack();
        this.setCenterLocation();

        this.parentFrame = parent;
        this.mainFrame = parent.getMainFrame();
        this.behaviorName = behaviorName;
        this.keyStroke = getClickedShortcutAccelerator(accelerator);
    }

    /**
     * Sets ask for shortcut dialog on screen's center
     */
    private void setCenterLocation()
    {
        setLocation((Toolkit.getDefaultToolkit().getScreenSize().width)/2 - getWidth()/2,
                (Toolkit.getDefaultToolkit().getScreenSize().height)/2 - getHeight()/2);
    }

    /**
     * Creates JPanel for needed components
     * @return JPanel
     */
    private JPanel askForShortcutDialog()
    {
        final JPanel jPanel = new JPanel();
        final JTextField pressKeyCombination = new JTextField();
        alreadyTaken = new JTextField();

        pressKeyCombination.setText(newShortcutInfo);
        pressKeyCombination.setEditable(false);
        pressKeyCombination.addKeyListener(this);

        alreadyTaken.setText(shortcutTakenAlert);
        alreadyTaken.setEditable(false);
        alreadyTaken.setVisible(false);


        jPanel.setLayout(new BorderLayout());
        jPanel.add(pressKeyCombination, BorderLayout.CENTER);
        jPanel.add(alreadyTaken, BorderLayout.SOUTH);

        return jPanel;
    }

    @Override
    public void keyTyped(final KeyEvent event)
    {
        // Implementation not needed
    }

    @Override
    public void keyPressed(final KeyEvent event)
    {
        if (!isModifierKey(event.getKeyCode()) && hasModifier(event.getModifiers()) && Objects.nonNull(mainFrame))
        {
            final JMenuItem menuItem = mainFrame.findMenuItem(keyStroke);
            final KeyStroke newAccelerator = KeyStroke.getKeyStrokeForEvent(event);
            final String stringShortcut = convertKeyStrokeToString(newAccelerator);
            final ResourceShortcutProvider shortcutProvider = ResourceShortcutProvider.getInstance();
            final String shortcutKey = shortcutProvider.getShortcutKeyByTranslation(behaviorName);
            final String tableFormShortcut = stringShortcut.replace(' ', '-').toUpperCase();

            if(!isShortcutAlreadyTaken(tableFormShortcut)) {
                menuItem.setAccelerator(newAccelerator);
                shortcutProvider.updateShortcut(behaviorName, tableFormShortcut);
                userPreferencesService.addShortcutPreference(shortcutKey, stringShortcut);

                redrawParentFrame();
                dispose();
            }
            else
            {
                alreadyTaken.setVisible(true);
                redrawAskForNewShortcutDialog();
            }
        }
    }

    /**
     * Redraws this dialog window
     */
    private void redrawAskForNewShortcutDialog()
    {
        this.repaint();
        this.revalidate();
    }

    /**
     * Updates shortcut dialog with the new shortcut
     */
    private void redrawParentFrame() {
        final JPanel shortcutPanel = parentFrame.getShortcutPanel();
        parentFrame.remove(shortcutPanel);
        parentFrame.setShortcutPanel(null);

        final JPanel newShortcutPanel = parentFrame.buildShortcutPanel();
        parentFrame.setShortcutPanel(newShortcutPanel);
        parentFrame.add(newShortcutPanel);
        parentFrame.repaint();
        parentFrame.revalidate();
    }

    @Override
    public void keyReleased(final KeyEvent event)
    {
        // Implementation not needed
    }

    /**
     * Converts string shortcut form from ShortcutDialog to keyStroke
     * @param accelerator given string shortcut
     * @return keystroke
     */
    private static KeyStroke getClickedShortcutAccelerator(final String accelerator)
    {
        return KeyStroke.getKeyStroke(accelerator.replace('-', ' ')
                .replace("CTRL", "ctrl")
                .replace("SHIFT", "shift")
                .replace("ALT", "alt"));
    }

    /**
     * Checks if given key combination is using modifier key (ctrl, shift, alt)
     * @param modifier given modifier from key combination
     * @return boolean
     */
    private static boolean hasModifier(final int modifier)
    {
        return modifier != 0;
    }

    /**
     * Checks if given keyCode is modifier key (ctrl, shift, alt)
     * @param keyCode given key code
     * @return boolean
     */
    private static boolean isModifierKey(final int keyCode)
    {
        final int SHIFT_KEYCODE = 16;
        final int CTRL_KEYCODE = 17;
        final int ALT_KEYCODE = 18;
        return keyCode == SHIFT_KEYCODE || keyCode == CTRL_KEYCODE || keyCode == ALT_KEYCODE;
    }

    /**
     * Converts keystroke to string form used in ShortcutDialog
     * @param keyStroke given accelerator
     * @return string form of keystroke
     */
    private static String convertKeyStrokeToString(final KeyStroke keyStroke)
    {
        final String keyStrokeAsString = keyStroke.toString();
        final String[] splittedShortcut = keyStrokeAsString.split(" ");
        final StringJoiner stringShortcut = new StringJoiner(" ");

        for (String shortcutPart : splittedShortcut)
        {
            if(!"pressed".equals(shortcutPart))
            {
                stringShortcut.add(shortcutPart);
            }
        }
        return stringShortcut.toString();
    }

    /**
     * Checks if new key combination is already taken by another action
     * @param newShortcut new key combination
     * @return boolean
     */
    private static boolean isShortcutAlreadyTaken(final String newShortcut)
    {
        final HashMap<String, String> allShortcuts = ResourceShortcutProvider.getInstance().getAllShortcuts();
        for(Map.Entry<String, String> entry : allShortcuts.entrySet())
        {
            String value = entry.getValue();
            if(value.equals(newShortcut))
            {
                return true;
            }
        }
        return false;
    }
}
