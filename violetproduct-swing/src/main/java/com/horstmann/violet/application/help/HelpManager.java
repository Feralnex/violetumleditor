package com.horstmann.violet.application.help;

import com.horstmann.violet.framework.dialog.DialogFactory;
import com.horstmann.violet.framework.injection.bean.ManiocFramework.BeanInjector;
import com.horstmann.violet.framework.injection.bean.ManiocFramework.InjectedBean;
import com.horstmann.violet.framework.injection.resources.ResourceBundleInjector;
import com.horstmann.violet.framework.injection.resources.annotation.ResourceBundleBean;
import com.horstmann.violet.framework.util.BrowserLauncher;
import com.horstmann.violet.framework.util.ClipboardPipe;

import javax.swing.*;
import java.awt.*;
import java.text.MessageFormat;

public final class HelpManager {
    private static HelpManager instance;
    @ResourceBundleBean(key = "help.userguide.url")
    private String userGuideURL;

    @ResourceBundleBean(key = "help.homepage.url")
    private String homePageURL;

    @ResourceBundleBean(key = "dialog.open_url_failed.ok")
    private String errorMessageTemplate;

    @ResourceBundleBean(key = "dialog.open_url_failed.title")
    private String errorDialogBoxTitle;

    @ResourceBundleBean(key = "dialog.open_url_failed.icon")
    private ImageIcon errorImageIcon;

    @InjectedBean
    private DialogFactory dialogFactory;


    private HelpManager() {
        ResourceBundleInjector.getInjector().inject(this);
        BeanInjector.getInjector().inject(this);
    }

    public static HelpManager getInstance() {
        if (instance == null) {
            instance = new HelpManager();
        }
        return instance;
    }

    /**
     * Opens online help
     */
    public void openUserGuide() {
        boolean isOK = BrowserLauncher.openURL(this.userGuideURL);
        if (!isOK) {
            showErrorDialog(this.userGuideURL);
        }
    }

    /**
     * Goes to homepage
     */
    public void openHomepage() {
        boolean isOK = BrowserLauncher.openURL(this.homePageURL);
        if (!isOK) {
            showErrorDialog(this.homePageURL);
        }
    }

    /**
     * Launch web browser or copy url to clipoard if failed
     * 
     * @param url
     */
    private void showErrorDialog(String url) {
        String message = MessageFormat.format(this.errorMessageTemplate, url);
        ClipboardPipe pipe = new ClipboardPipe(url);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(pipe, null);
        dialogFactory.showErrorDialog(message);
    }
}
