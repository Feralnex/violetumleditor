package com.horstmann.violet.application.gui;

import com.horstmann.violet.framework.injection.bean.ManiocFramework.BeanInjector;
import com.horstmann.violet.framework.injection.bean.ManiocFramework.InjectedBean;
import com.horstmann.violet.framework.injection.resources.ResourceBundleInjector;
import com.horstmann.violet.framework.injection.resources.annotation.ResourceBundleBean;
import com.horstmann.violet.framework.userpreferences.UserPreferencesService;

import javax.swing.*;
import java.awt.*;

public class MarginDialog extends JDialog {
    @ResourceBundleBean(key="margin.dialog.title")
    private String dialogTitle;

    @ResourceBundleBean(key="margin.dialog.top")
    private String marginTop;

    @ResourceBundleBean(key="margin.dialog.bottom")
    private String marginBottom;

    @ResourceBundleBean(key="margin.dialog.left")
    private String marginLeft;

    @ResourceBundleBean(key="margin.dialog.right")
    private String marginRight;

    @ResourceBundleBean(key="margin.dialog.button.save")
    private String buttonSaveText;

    @InjectedBean
    private UserPreferencesService userPreferencesService;

    private JPanel marginPanel;

    /**
     * Creates a MarginDialog
     * @param parent JFrame parent
     */
    public MarginDialog(JFrame parent)
    {
        super(parent);
        ResourceBundleInjector.getInjector().inject(this);
        BeanInjector.getInjector().inject(this);

        this.setTitle(dialogTitle);
        this.setLocationRelativeTo(null);
        this.setModal(true);
        this.setResizable(false);
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.getContentPane().add(buildMarginPanel());
        pack();
        setCenterLocation(parent);
    }

    /**
     * Prepares a JPanel used to get margins values from user.
     *
     * @return The built JPanel
     */
    private JPanel buildMarginPanel() {
        if(marginPanel == null)
        {
            marginPanel = new JPanel(new GridLayout(5,2));

            Insets currentMargins = userPreferencesService.getMargins();

            SpinnerModel spinnerModelTop = new SpinnerNumberModel(currentMargins.top,0,1000, 1);
            SpinnerModel spinnerModelBottom = new SpinnerNumberModel(currentMargins.bottom,0,1000, 1);
            SpinnerModel spinnerModelLeft = new SpinnerNumberModel(currentMargins.left,0,1000, 1);
            SpinnerModel spinnerModelRight = new SpinnerNumberModel(currentMargins.right,0,1000, 1);

            JSpinner topSpinner = new JSpinner(spinnerModelTop);
            topSpinner.setEditor(new JSpinner.NumberEditor(topSpinner, "# px"));
            JSpinner bottomSpinner = new JSpinner(spinnerModelBottom);
            bottomSpinner.setEditor(new JSpinner.NumberEditor(bottomSpinner, "# px"));
            JSpinner leftSpinner = new JSpinner(spinnerModelLeft);
            leftSpinner.setEditor(new JSpinner.NumberEditor(leftSpinner, "# px"));
            JSpinner rightSpinner = new JSpinner(spinnerModelRight);
            rightSpinner.setEditor(new JSpinner.NumberEditor(rightSpinner, "# px"));

            JLabel topLabel = new JLabel(marginTop);
            JLabel bottomLabel = new JLabel(marginBottom);
            JLabel leftLabel = new JLabel(marginLeft);
            JLabel rightLabel = new JLabel(marginRight);

            JButton buttonSave = new JButton(buttonSaveText);
            buttonSave.addActionListener(e -> {
                Insets newMargins = new Insets(
                        (Integer)topSpinner.getValue(),
                        (Integer)leftSpinner.getValue(),
                        (Integer)bottomSpinner.getValue(),
                        (Integer)rightSpinner.getValue()
                );
                userPreferencesService.setMargins(newMargins);
                dispose();
            });


            marginPanel.add(topLabel);
            marginPanel.add(topSpinner);
            marginPanel.add(bottomLabel);
            marginPanel.add(bottomSpinner);
            marginPanel.add(leftLabel);
            marginPanel.add(leftSpinner);
            marginPanel.add(rightLabel);
            marginPanel.add(rightSpinner);
            marginPanel.add(buttonSave);
        }
        return this.marginPanel;
    }

    /**
     * Sets the Dialog location to the center of the parent JFrame
     * @param parent
     */
    private void setCenterLocation(JFrame parent)
    {
        setLocation((parent.getWidth() - getWidth()) / 2, (parent.getHeight() - getHeight()) / 2);
    }
}
