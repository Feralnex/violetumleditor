package com.horstmann.violet.application;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import com.horstmann.violet.application.gui.MainFrame;
import com.horstmann.violet.framework.dialog.DialogFactory;
import com.horstmann.violet.framework.file.IGraphFile;
import com.horstmann.violet.framework.injection.bean.ManiocFramework.BeanInjector;
import com.horstmann.violet.framework.injection.bean.ManiocFramework.InjectedBean;
import com.horstmann.violet.framework.injection.resources.ResourceBundleInjector;
import com.horstmann.violet.framework.injection.resources.annotation.ResourceBundleBean;
import com.horstmann.violet.framework.userpreferences.UserPreferencesService;
import com.horstmann.violet.workspace.IWorkspace;

public class ApplicationStopper
{

    public ApplicationStopper()
    {
        BeanInjector.getInjector().inject(this);
        ResourceBundleInjector.getInjector().inject(this);
    }

    /**
     * Exits the program if no graphs have been modified or if the user agrees to abandon modified graphs or save its.
     */
    public void exitProgram(MainFrame mainFrame) {
        if (mainFrame == null) System.exit(0);

        List<IWorkspace> unsavedWorkspaces = getUnsavedWorkspaces(mainFrame.getWorkspaceList());
        boolean readyToExit = isItReadyToExit(mainFrame, unsavedWorkspaces);
        if (!readyToExit) return;

        PerformPreExit(mainFrame);

        System.exit(0);
    }

    private void PerformPreExit(MainFrame mainFrame) {
        for (IWorkspace workspace : mainFrame.getWorkspaceList()) {
            // Perform what you want here before exit!!!
        }
    }

    /**
     * Asks user to save changes before exit.
     *
     * @return true is all is saved either false
     */
    private boolean isItReadyToExit(MainFrame mainFrame, List<IWorkspace> unsavedWorkspaces) {
        int unsavedWorkspacesCount = unsavedWorkspaces.size();

        boolean unsavedChecked = isThereAnyUnsavedWorkspace(unsavedWorkspaces);

        IWorkspace activeWorkspace = mainFrame.getActiveWorkspace();
        if (unsavedChecked) {

            JOptionPane closingConfirmationPane = createClosingConfirmationPaneFor(unsavedWorkspacesCount);
            dialogFactory.showDialog(closingConfirmationPane, this.dialogExitTitle, true);

            int chosenOption = getChosenOption(closingConfirmationPane);

            switch (chosenOption) {
                case (JOptionPane.YES_OPTION):
                    saveUnsavedWorkspaces(unsavedWorkspaces);

                    unsavedWorkspaces = getUnsavedWorkspaces(mainFrame.getWorkspaceList());
                    unsavedChecked = isThereAnyUnsavedWorkspace(unsavedWorkspaces);

                    if (unsavedChecked) {
                        return false;
                    }

                    if (activeWorkspace != null) {
                        this.userPreferencesService.setActiveDiagramFile(activeWorkspace.getGraphFile());
                    }
                    return true;

                case (JOptionPane.NO_OPTION):
                    if (activeWorkspace != null) {
                        this.userPreferencesService.setActiveDiagramFile(activeWorkspace.getGraphFile());
                    }
                    return true;

                default:
                    return false;
            }
        }
        if (activeWorkspace != null) {
            this.userPreferencesService.setActiveDiagramFile(activeWorkspace.getGraphFile());
        }
        return true;
    }

    /**
     * Creates closing confirmation pane
     *
     * @return reference to created pane
     */
    private JOptionPane createClosingConfirmationPaneFor(int unsavedWorkspacesCount) {
        String message = MessageFormat.format(this.dialogExitMessage, new Object[] {
                new Integer(unsavedWorkspacesCount)
        });
        JOptionPane optionPane = new JOptionPane(message, JOptionPane.CLOSED_OPTION, JOptionPane.YES_NO_CANCEL_OPTION, this.dialogExitIcon);
        return optionPane;
    }

    private boolean isThereAnyUnsavedWorkspace(List<IWorkspace> dirtyWorkspaceList) {
        int unsavedCount;
        unsavedCount = dirtyWorkspaceList.size();
        if (unsavedCount > 0) {
            return true;
        }
        return false;
    }

    private List<IWorkspace> getUnsavedWorkspaces(List<IWorkspace> workspaceList){
        List<IWorkspace> unsavedWorkspaces = new ArrayList<IWorkspace>();
        for (IWorkspace workspace: workspaceList) {
            IGraphFile graphFile = workspace.getGraphFile();
            if (graphFile.isSaveRequired()) {
                unsavedWorkspaces.add(workspace);
            }
        }
        return unsavedWorkspaces;
    }

    private int getChosenOption(JOptionPane optionPane) {
        if (JOptionPane.UNINITIALIZED_VALUE.equals(optionPane.getValue())) {
            return JOptionPane.YES_OPTION;
        } else {
            return ((Integer) optionPane.getValue()).intValue();
        }
    }

    private void saveUnsavedWorkspaces(List<IWorkspace> unsavedWorkspaces) {
        for (IWorkspace aDirtyWorkspace : unsavedWorkspaces) {
            aDirtyWorkspace.getGraphFile().save();
        }
    }

    @ResourceBundleBean(key = "dialog.exit.icon")
    private ImageIcon dialogExitIcon;

    @ResourceBundleBean(key = "dialog.exit.ok")
    private String dialogExitMessage;

    @ResourceBundleBean(key = "dialog.exit.title")
    private String dialogExitTitle;

    @InjectedBean
    private DialogFactory dialogFactory;

    @InjectedBean
    private UserPreferencesService userPreferencesService;
}
