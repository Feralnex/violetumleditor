package com.horstmann.violet.application.gui;

import com.horstmann.violet.application.menu.FileMenu;
import com.horstmann.violet.workspace.IWorkspace;
import com.horstmann.violet.workspace.WorkspacePanel;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.Objects;

public class TabbedPanel extends JTabbedPane
{
    private MainFrame mainFrame;
    private transient List<IWorkspace> workspaceList;
    private JPanel startPanel;

    /**
     * Creates default start tab/page
     * @param mainFrame for accesing workspace list and updating documents menu
     */
    TabbedPanel(final MainFrame mainFrame)
    {
        this.mainFrame = mainFrame;
        this.workspaceList = mainFrame.getWorkspaceList();

        final FileMenu fileMenu = mainFrame.getMenuFactory().getFileMenu(mainFrame);
        startPanel = new WelcomePanel(fileMenu);
        add(startPanel);
        setTabComponentAt(indexOfComponent(startPanel), prepareTitlePanel("Start", null));
    }

    /**
     * Updates tabs when the new one arrives
     */
    void updateTabbedPanel() {
        if (Objects.nonNull(startPanel))
        {
            remove(startPanel);
            startPanel = null;
        }

        workspaceList.forEach(workspace ->
        {
            final String workspaceTitle = workspace.getTitle();
            final WorkspacePanel workspaceAWTComponent = workspace.getAWTComponent();
            add(workspaceAWTComponent);
            final int indexOfComponent = indexOfComponent(workspaceAWTComponent);
            setTabComponentAt(indexOfComponent, prepareTitlePanel(workspaceTitle, workspace));
        });
    }

    /**
     * Prepares tab title element
     * @param title - workspace title
     * @param workspace workspace
     * @return panel with title and close button
     */
    private JPanel prepareTitlePanel(String title, IWorkspace workspace)
    {
        JPanel tabPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        tabPanel.setOpaque(false);
        JLabel titleLabel = new JLabel(title);
        titleLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));
        tabPanel.add(titleLabel);

        JButton closeButton = new JButton("\u274C");
        closeButton.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
        closeButton.setContentAreaFilled(false);
        closeButton.addActionListener(event -> {
            if (Objects.nonNull(workspace)) {
                remove(workspace.getAWTComponent());
                workspaceList.remove(workspace);
                mainFrame.getMenuFactory().getDocumentMenu(mainFrame).updateMenuItem();
            } else {
                remove(startPanel);
            }
        });
        tabPanel.add(closeButton);

        return tabPanel;
    }

    /**
     * Changes tab title
     * @param workspace given workspace
     * @param newTitle new title
     */
    public void changeTabTitle(final IWorkspace workspace, final String newTitle) {
        final int indexOfComponent = indexOfComponent(workspace.getAWTComponent());
        setTabComponentAt(indexOfComponent, prepareTitlePanel(newTitle, workspace));
    }
}
