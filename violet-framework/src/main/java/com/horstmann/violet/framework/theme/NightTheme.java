/*
Violet - A program for editing UML diagrams.

Copyright (C) 2007 Cay S. Horstmann (http://horstmann.com)
                   Alexandre de Pellegrin (http://alexdp.free.fr);

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package com.horstmann.violet.framework.theme;

import com.horstmann.violet.workspace.editorpart.PlainGrid;
import com.pagosoft.plaf.PgsLookAndFeel;
import com.pagosoft.plaf.PgsTheme;
import com.pagosoft.plaf.PlafOptions;

import javax.swing.*;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.metal.MetalLookAndFeel;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Implements Vista Nigjt theme
 * 
 * @author Alexandre de Pellegrin
 * 
 */
public class NightTheme extends AbstractTheme
{

	@Override
	public ThemeInfo getThemeInfo() {
		return new ThemeInfo("Night Theme", NightTheme.class, PgsLookAndFeel.class);
	}
	
    @Override
    protected void configure()
    {
    	UIDefaults defaults = UIManager.getDefaults();
        Map<String, Object> m = new HashMap<String, Object>();
        m.put("MenuItem.background", new Color(60, 63, 65));
        m.put("MenuBar.background", new Color(74, 74, 74));
        defaults.putAll(m);
        BlackTheme vistaTheme = new BlackTheme()
        {
            public ColorUIResource getMenuBackground()
            {
                return new ColorUIResource(new Color(60,63,65));
            }


            public ColorUIResource getSecondary3()
            {
                return new ColorUIResource(new Color(60, 63, 65));
            }
        };

        PgsLookAndFeel.setCurrentTheme(vistaTheme);
    }



    public Color getWhiteColor()
    {
        return Color.WHITE;
    }

    public Color getBlackColor()
    {
        return Color.BLACK;
    }

    public Color getGridColor()
    {
        return new Color(94,94,94);
    }

    public Color getMainBackGroundColor()
    {
        return new Color(94,94,94);
    }

    public Color getBackgroundColor()
    {
        return new Color (60,63,65);
    }

    public Font getMenubarFont()
    {
        return MetalLookAndFeel.getMenuTextFont();
    }

    public Color getMenubarBackgroundColor()
    {
        return new Color(60,63,65);
    }

    public Color getMenubarForegroundColor()
    {
        return new Color(90,93,94);
    }

    public Color getRolloverButtonDefaultColor()
    {
        return new Color(111,111,111);
    }

    public Color getRolloverButtonRolloverBorderColor()
    {
        return getMenubarForegroundColor();
    }

    public Color getRolloverButtonRolloverColor()
    {
        return new Color(83,83,83);
    }

    public Color getSidebarBackgroundEndColor()
    {
        return new Color(94,95,96);
    }

    public Color getSidebarBackgroundStartColor()
    {
        return new Color(90, 92, 114);
    }

    public Color getSidebarBorderColor()
    {
        return getBackgroundColor();
    }

    public Color getSidebarElementBackgroundColor()
    {
        return getBackgroundColor();
    }

    public Color getSidebarElementTitleBackgroundEndColor()
    {
        return new Color(94,95,96);
    }

    public Color getSidebarElementTitleBackgroundStartColor()
    {
        return new Color(74,76,78);
    }

    public Color getSidebarElementForegroundColor()
    {
        return new Color(188, 188, 188);
    }

    public Color getSidebarElementTitleOverColor()
    {
        return new Color(200,201,199);
    }

    public Color getStatusbarBackgroundColor()
    {
        return new Color(94,95,96);
    }

    public Color getStatusbarBorderColor()
    {
        return getMenubarBackgroundColor();
    }

    public Font getToggleButtonFont()
    {
        return MetalLookAndFeel.getMenuTextFont().deriveFont(Font.PLAIN);
    }

    public Color getToggleButtonSelectedBorderColor()
    {
        return new Color(60,63,65);
    }

    public Color getToggleButtonSelectedColor()
    {
        return new Color(60,63,65);
    }

    public Color getToggleButtonUnselectedColor()
    {
        return getSidebarElementBackgroundColor();
    }

    public Font getWelcomeBigFont()
    {
        return MetalLookAndFeel.getWindowTitleFont().deriveFont((float) 28.0);
    }

    public Font getWelcomeSmallFont()
    {
        return MetalLookAndFeel.getWindowTitleFont().deriveFont((float) 12.0).deriveFont(Font.PLAIN);
    }

    public Color getWelcomeBackgroundEndColor()
    {
        return new Color(90,90,90);
    }

    public Color getWelcomeBackgroundStartColor()
    {
        return new Color(60,63,65);
    }

    public Color getWelcomeBigForegroundColor()
    {
        return new Color(120,120,120);
    }

    public Color getWelcomeBigRolloverForegroundColor()
    {
        return new Color(170,170,170);
    }

    
    private class BlackTheme extends PgsTheme
    {
        public BlackTheme()
        {
            super("Black");

            setSecondary3(new ColorUIResource(new Color(50, 50, 50)));
            setSecondary2(new ColorUIResource(50,50,50));
            setSecondary1(new ColorUIResource(50,50,50));

            setPrimary1(new ColorUIResource(200,200,200)); // obwódka przy podświetleniu w menu jak najedziemy myszką, napis skrótów klawiszowych
            setPrimary2(new ColorUIResource(100,103,105)); //obwódka przy zapisie pliku, buttona w właściwościach
            setPrimary3(new ColorUIResource(44,41,39)); // podświetlenie w menu jak najedziemy myszką

            setBlack(new ColorUIResource(133, 158, 225));
            setWhite(new ColorUIResource(187,187,187));

            PlafOptions.setOfficeScrollBarEnabled(true);
            PlafOptions.setVistaStyle(true);
            PlafOptions.useBoldFonts(false);


            setDefaults(new Object[]
            {
                    "MenuBar.isFlat",
                    Boolean.FALSE,
                    "MenuBar.gradientStart", // TLO PANELU GORNEGO - MENU
                    new ColorUIResource(83, 83, 83),
                    "MenuBar.gradientMiddle",
                    new ColorUIResource(83, 83, 83),
                    "MenuBar.gradientEnd",
                    new ColorUIResource(83, 83, 83),

                    "MenuBarMenu.isFlat",
                    Boolean.FALSE,
                    "MenuBarMenu.foreground",
                    getWhite(),
                    "MenuBarMenu.rolloverBackground.gradientStart", //PODSWIETLENIE GDY NAJEDZIEMY NA BUTTON Z MENU
                    new ColorUIResource(111, 111, 111),
                    "MenuBarMenu.rolloverBackground.gradientMiddle",
                    new ColorUIResource(106, 106, 106),
                    "MenuBarMenu.rolloverBackground.gradientEnd",
                    new ColorUIResource(101, 101, 101),
                    "MenuBarMenu.selectedBackground.gradientStart",
                    new ColorUIResource(57, 57, 57),    //PODSWIETLENIE GDY NACISNIEMY PRZYCISK Z MENU GLOWNEGO
                    "MenuBarMenu.selectedBackground.gradientMiddle",
                    new ColorUIResource(57, 57, 57),
                    "MenuBarMenu.selectedBackground.gradientEnd",
                    new ColorUIResource(57, 57, 57),
                    "MenuBarMenu.rolloverBorderColor",
                    new ColorUIResource(44, 44, 44), // OBWÓDKA po najechaniu na przycisk z menu glownego
                    "MenuBarMenu.selectedBorderColor",
                    new ColorUIResource(44, 44, 44), // OBWÓDKA po nacisnieciu na przycisk z menu glownego

                    "Menu.gradientStart",
                    getPrimary3(), //kolor po najechaniu kursorem na rozwiniecie w prawo, w menu
                    "Menu.gradientEnd",
                    getPrimary3(), //kolor po najechaniu kursorem na rozwiniecie w prawo, w menu
                    "Menu.gradientMiddle",
                    getPrimary3(), //kolor po najechaniu kursorem na rozwiniecie w prawo, w menu
                    "Menu.isFlat",
                    Boolean.FALSE,

                    "MenuItem.gradientStart", //kolor po najechaniu kursorem w menu
                    getPrimary3(),
                    "MenuItem.gradientEnd",
                    getPrimary3(),
                    "MenuItem.gradientMiddle",
                    getPrimary3(),
                    "MenuItem.isFlat",
                    Boolean.FALSE,

                    "CheckBoxMenuItem.gradientStart", //kolor po najechaniu kursorem w menu
                    getPrimary3(),
                    "CheckBoxMenuItem.gradientEnd",
                    getPrimary3(),
                    "CheckBoxMenuItem.gradientMiddle",
                    getPrimary3(),
                    "CheckBoxMenuItem.isFlat",
                    Boolean.FALSE,

                    "RadioButtonMenuItem.gradientStart", //kolor po najechaniu kursorem w menu
                    getPrimary3(),
                    "RadioButtonMenuItem.gradientEnd",
                    getPrimary3(),
                    "RadioButtonMenuItem.gradientMiddle",
                    getPrimary3(),
                    "RadioButtonMenuItem.isFlat",
                    Boolean.FALSE,

                    "Button.rolloverGradientStart",
                    getPrimary3(),
                    "Button.rolloverGradientEnd",
                    getPrimary3(),
                    "Button.selectedGradientStart",
                    getPrimary3(),
                    "Button.selectedGradientEnd",
                    getPrimary3(),
                    "Button.rolloverVistaStyle",
                    Boolean.TRUE,
                    "glow",
                    getPrimary1(),

                    "ToggleButton.rolloverGradientStart",
                    getPrimary3(),
                    "ToggleButton.rolloverGradientEnd",
                    getPrimary3(),
                    "ToggleButton.selectedGradientStart",
                    getPrimary3(),
                    "ToggleButton.selectedGradientEnd",
                    getPrimary3(),

                    "TabbedPane.selected",
                    new ColorUIResource(74, 74, 74),
                    "TabbedPane.background",
                    new ColorUIResource(74,74,74 ),
                    "TabbedPane.selectedForeground",
                    new ColorUIResource(74,74,74),


            });
        }
    }
    
}
