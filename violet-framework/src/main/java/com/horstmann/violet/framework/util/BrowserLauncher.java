/*
 Violet - A program for editing UML diagrams.

 Copyright (C) 2007 Cay S. Horstmann (http://horstmann.com)
 Alexandre de Pellegrin (http://alexdp.free.fr);

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package com.horstmann.violet.framework.util;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public final class BrowserLauncher
{
    private BrowserLauncher()
    {
        throw new IllegalStateException("Utility class");
    }

    public static boolean openURL(String url)
    {
        Desktop desktop = Desktop.getDesktop();
        URI uri;
        try
        {
            uri = new URI(url);
            desktop.browse(uri);
            return true;
        }
        catch (URISyntaxException | IOException | UnsupportedOperationException e)
        {
            e.printStackTrace();
            return false;
        }
    }

}