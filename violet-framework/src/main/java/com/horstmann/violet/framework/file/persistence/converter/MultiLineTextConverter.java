package com.horstmann.violet.framework.file.persistence.converter;

import com.horstmann.violet.product.diagram.property.text.MultiLineText;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * XStream converter for MultiLineText class
 */
public class MultiLineTextConverter implements Converter
{

    @Override
    public void marshal(Object object,
                        HierarchicalStreamWriter hierarchicalStreamWriter,
                        MarshallingContext marshallingContext)
    {
        MultiLineText multiLineText = (MultiLineText) object;
        hierarchicalStreamWriter.startNode("text");
        hierarchicalStreamWriter.setValue(multiLineText.toEdit());
        hierarchicalStreamWriter.endNode();
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader hierarchicalStreamReader,
                            UnmarshallingContext unmarshallingContext)
    {
        MultiLineText multiLineText = new MultiLineText();
        hierarchicalStreamReader.moveDown();
        multiLineText.setText(hierarchicalStreamReader.getValue());
        hierarchicalStreamReader.moveUp();
        return multiLineText;
    }

    @Override
    public boolean canConvert(Class aClass)
    {
        return aClass.equals(MultiLineText.class);
    }
}
