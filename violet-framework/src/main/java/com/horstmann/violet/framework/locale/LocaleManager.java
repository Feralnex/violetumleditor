package com.horstmann.violet.framework.locale;

import com.horstmann.violet.framework.injection.bean.ManiocFramework;
import com.horstmann.violet.framework.userpreferences.UserPreferencesService;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Manages locales
 */
@ManiocFramework.ManagedBean(registeredManually = true)
public class LocaleManager
{
    private static LocaleManager instance;
    private List<String> supportedLocaleCodes = new ArrayList<>();

    @ManiocFramework.InjectedBean
    private UserPreferencesService userPreferencesServices;

    public LocaleManager()
    {
        ManiocFramework.BeanInjector.getInjector().inject(this);
        initSupportedLocaleCodes();
        LocaleManager.setInstance(this);
    }

    private void initSupportedLocaleCodes()
    {
        for (final SupportedLocaleCode localeCode : SupportedLocaleCode.values())
        {
            supportedLocaleCodes.add(localeCode.name());
        }
    }

    public static LocaleManager getInstance()
    {
        return LocaleManager.instance;
    }

    private static void setInstance(LocaleManager localeManager)
    {
        LocaleManager.instance = localeManager;
    }

    public static void setLocale(final String localeCode)
    {
        if (instance.supportedLocaleCodes.contains(localeCode))
        {
            instance.userPreferencesServices.setPreferredLocale(localeCode);
            instance.userPreferencesServices.setUsePreferredLocale("true");
        }
        else
        {
            throw new IllegalArgumentException("Unsupported locale code: " + localeCode);
        }
    }

    public static String getCurrentLocaleCode() {
        final boolean userChosenLocale = Boolean.parseBoolean(instance.userPreferencesServices.getUsePreferredLocale());
        if (userChosenLocale) {
            return instance.userPreferencesServices.getPreferredLocale();
        } else {
            final String languageCode = Locale.getDefault().getLanguage();
            return languageCode.toUpperCase();
        }
    }

    public static List<String> getSupportedLocaleCodes() {
        return instance.supportedLocaleCodes;
    }

    private enum SupportedLocaleCode
    {
        EN, PL, FR, DE
    }
}
