/*
 Violet - A program for editing UML diagrams.

 Copyright (C) 2007 Cay S. Horstmann (http://horstmann.com)
 Alexandre de Pellegrin (http://alexdp.free.fr);

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package com.horstmann.violet.framework.userpreferences;

import java.util.Arrays;
import java.util.MissingResourceException;

/**
 * Preferences constants. (Pattern to avoid use of string to store preferences)
 * 
 * @author Alexandre de Pellegrin
 * 
 */
public class PreferencesConstant
{

    /**
     * Default constructor
     * 
     * @param key
     */
    private PreferencesConstant(String key)
    {
        this.key = key;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    public String toString()
    {
        return this.key;
    }

    /**
     * Constant key
     */
    private String key;

    /**
     * Key to store prefered look and feel
     */
    public static final PreferencesConstant LOOK_AND_FEEL = new PreferencesConstant("look_and_feel");

    /**
     * Key to store state of autosave
     */

    public static final PreferencesConstant AUTOSAVE = new PreferencesConstant("autosave");

    /**
     * Key to store recently opened files
     */
    public static final PreferencesConstant RECENT_FILES = new PreferencesConstant("recent");

    /**
     * Key to store files that are currently opened (usefull to restore workspace state on next session
     */
    public static final PreferencesConstant OPENED_FILES_ON_WORKSPACE = new PreferencesConstant("opened");

    /**
     * Constant just used as file separator for files
     */
    public static final PreferencesConstant FILE_SEPARATOR = new PreferencesConstant("\n");
    
    /**
     * Constant to separate directory and filename
     */
    public static final PreferencesConstant PATH_SEPARATOR = new PreferencesConstant(" -> ");

    /**
     * Key to store selected file on workspace
     */
    public static final PreferencesConstant ACTIVE_FILE = new PreferencesConstant("active");

    /**
     * Key to store selected file on workspace
     */
    public static final PreferencesConstant FONT_SIZE = new PreferencesConstant("fontSize");

    /**
     * Key to store user id for peer-to-peer mode.
     * Only for for host config (in other words, program instance hosting shared
     * documents)
     */
    public static final PreferencesConstant NETWORK_HOSTCONFIG_USERID =
            new PreferencesConstant("network.hostconfig.userid");

    /**
     * Key to store user id for peer-to-peer mode.
     * Only for for guest config (in other words, to access to remote documents)
     */
    public static final PreferencesConstant NETWORK_GUESTCONFIG_USERID
            = new PreferencesConstant("network.guestconfig.userid");

    /**
     * Key to store SERVER http url for peer-to-peer mode.
     * Only for for guest config (in other words, to access to remote documents)
     */
    public static final PreferencesConstant NETWORK_GUESTCONFIG_HTTP_SERVERURL = new PreferencesConstant(
            "network.guestconfig.http.serverurl");


    public static final PreferencesConstant MARGINS = new PreferencesConstant(
            "margins"
    );
    

    /**

     * Keys to store user shortcuts
     */
    public static final PreferencesConstant SHORTCUT_FILE_OPEN = new PreferencesConstant("file.open");
    public static final PreferencesConstant SHORTCUT_FILE_OPEN_REMOTE = new PreferencesConstant("file.open_remote");
    public static final PreferencesConstant SHORTCUT_FILE_CLOSE = new PreferencesConstant("file.close");
    public static final PreferencesConstant SHORTCUT_FILE_SAVE = new PreferencesConstant("file.save");
    public static final PreferencesConstant SHORTCUT_FILE_EXIT = new PreferencesConstant("file.exit");

    public static final PreferencesConstant SHORTCUT_EDIT_UNDO = new PreferencesConstant("edit.undo");
    public static final PreferencesConstant SHORTCUT_EDIT_REDO = new PreferencesConstant("edit.redo");
    public static final PreferencesConstant SHORTCUT_EDIT_PROPERTIES = new PreferencesConstant("edit.properties");
    public static final PreferencesConstant SHORTCUT_EDIT_DELETE = new PreferencesConstant("edit.delete");
    public static final PreferencesConstant SHORTCUT_EDIT_CUT = new PreferencesConstant("edit.cut");
    public static final PreferencesConstant SHORTCUT_EDIT_COPY = new PreferencesConstant("edit.copy");
    public static final PreferencesConstant SHORTCUT_EDIT_PASTE = new PreferencesConstant("edit.paste");
    public static final PreferencesConstant SHORTCUT_EDIT_INVERT = new PreferencesConstant("edit.invert");
    public static final PreferencesConstant SHORTCUT_EDIT_SELECT_ALL = new PreferencesConstant("edit.select_all");
    public static final PreferencesConstant SHORTCUT_EDIT_SELECT_NEXT = new PreferencesConstant("edit.select_next");
    public static final PreferencesConstant SHORTCUT_EDIT_SELECT_PREV = new PreferencesConstant("edit.select_previous");

    public static final PreferencesConstant SHORTCUT_VIEW_ZOOM_OUT = new PreferencesConstant("view.zoom_out");
    public static final PreferencesConstant SHORTCUT_VIEW_ZOOM_IN = new PreferencesConstant("view.zoom_in");

    public static final PreferencesConstant SHORTCUT_WINDOW_NEXT = new PreferencesConstant("window.next");
    public static final PreferencesConstant SHORTCUT_WINDOW_PREV = new PreferencesConstant("window.previous");
    public static final PreferencesConstant SHORTCUT_WINDOW_CLOSE = new PreferencesConstant("window.close");

    /*
     * Key to store flag for when to use stored locale
     */
    public static final PreferencesConstant USE_LOCALE = new PreferencesConstant("use_locale");

    /**
     * Key to store user preferred locale
     */
    public static final PreferencesConstant LOCALE = new PreferencesConstant("locale");



    /**
     * Preference constants list
     */
    public static final PreferencesConstant[] LIST;

    static
    {



        LIST = new PreferencesConstant[32];



        LIST[0] = LOOK_AND_FEEL;
        LIST[1] = RECENT_FILES;
        LIST[2] = OPENED_FILES_ON_WORKSPACE;
        LIST[3] = ACTIVE_FILE;
        LIST[4] = NETWORK_HOSTCONFIG_USERID;
        LIST[5] = NETWORK_GUESTCONFIG_USERID;
        LIST[6] = NETWORK_GUESTCONFIG_HTTP_SERVERURL;
        LIST[7] = SHORTCUT_FILE_OPEN;
        LIST[8] = SHORTCUT_FILE_OPEN_REMOTE;
        LIST[9] = SHORTCUT_FILE_CLOSE;
        LIST[10] = SHORTCUT_FILE_SAVE;
        LIST[11] = SHORTCUT_FILE_EXIT;
        LIST[12] = SHORTCUT_EDIT_UNDO;
        LIST[13] = SHORTCUT_EDIT_REDO;
        LIST[14] = SHORTCUT_EDIT_PROPERTIES;
        LIST[15] = SHORTCUT_EDIT_DELETE;
        LIST[16] = SHORTCUT_EDIT_CUT;
        LIST[17] = SHORTCUT_EDIT_COPY;
        LIST[18] = SHORTCUT_EDIT_PASTE;
        LIST[19] = SHORTCUT_EDIT_SELECT_ALL;
        LIST[20] = SHORTCUT_EDIT_SELECT_NEXT;
        LIST[21] = SHORTCUT_EDIT_SELECT_PREV;
        LIST[22] = SHORTCUT_VIEW_ZOOM_IN;
        LIST[23] = SHORTCUT_VIEW_ZOOM_OUT;
        LIST[24] = SHORTCUT_WINDOW_NEXT;
        LIST[25] = SHORTCUT_WINDOW_PREV;
        LIST[26] = SHORTCUT_WINDOW_CLOSE;
        LIST[27] = USE_LOCALE;
        LIST[28] = LOCALE;
        LIST[29] = FONT_SIZE;
        LIST[30] = MARGINS;
        LIST[31] = SHORTCUT_EDIT_INVERT;
    }

    static PreferencesConstant getShortcutPreferenceByKey(final String key) {
        return Arrays.stream(LIST).filter(shortcut -> shortcut.toString().equals(key))
                .findFirst()
                .orElseThrow(() -> new MissingResourceException("Missing preference",
                        PreferencesConstant.class.getName(),
                        key));

    }

      
}
