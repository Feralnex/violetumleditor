package com.horstmann.violet.framework.file.persistence.converter;

import com.horstmann.violet.product.diagram.property.text.SingleLineText;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * XStream converter for SingleLineText class
 */
public class SingleLineTextConverter implements Converter
{

    @Override
    public void marshal(Object object,
                        HierarchicalStreamWriter hierarchicalStreamWriter,
                        MarshallingContext marshallingContext)
    {
        SingleLineText singleLineText = (SingleLineText) object;
        hierarchicalStreamWriter.startNode("text");
        hierarchicalStreamWriter.setValue(singleLineText.toString());
        hierarchicalStreamWriter.endNode();
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader hierarchicalStreamReader,
                            UnmarshallingContext unmarshallingContext)
    {
        SingleLineText singleLineText = new SingleLineText();
        hierarchicalStreamReader.moveDown();
        singleLineText.setText(hierarchicalStreamReader.getValue());
        hierarchicalStreamReader.moveUp();
        return singleLineText;
    }

    @Override
    public boolean canConvert(Class aClass)
    {
        return aClass.equals(SingleLineText.class);
    }
}
