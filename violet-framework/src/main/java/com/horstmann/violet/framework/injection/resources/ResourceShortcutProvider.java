package com.horstmann.violet.framework.injection.resources;

import java.util.HashMap;

/**
 * A singleton class, where stores all shortcut, that was injected by ResourceFactory
 */
public class ResourceShortcutProvider {
    private HashMap<String, String> shortcutsMap;
    private HashMap<String, String> shortcutTranslationMap;

    private ResourceShortcutProvider() {
        shortcutsMap = new HashMap<>();
        shortcutTranslationMap = new HashMap<>();
    }

    private static final class SingletonHolder {
        private static ResourceShortcutProvider INSTANCE = new ResourceShortcutProvider();
    }

    /**
     * Return instance of ResourceShortcutProvider.
     *
     * @return instance of ResourceShortcutProvider
     */
    public static ResourceShortcutProvider getInstance() {
        return SingletonHolder.INSTANCE;
    }

    /**
     * Adding shortcut to HashMap if behavior name didn't exist
     *
     * @param behaviorName     shortcut name
     * @param behaviorShortcut shortcut
     */
    public void addShortcut(String behaviorName, String behaviorShortcut) {
        if (!shortcutsMap.containsKey(behaviorName)) {
            shortcutsMap.put(behaviorName, behaviorShortcut);
        }
    }

    /**
     * Return HashMap of all register shortcuts
     *
     * @return HashMap of shortcuts
     */
    public HashMap<String, String> getAllShortcuts() {
        return shortcutsMap;
    }

    /**
     * Updates given shortcut
     *
     * @param behaviorName     shorcut name
     * @param behaviorShortcut new shortcut
     */
    public void updateShortcut(final String behaviorName, final String behaviorShortcut) {
        if (shortcutsMap.containsKey(behaviorName)) {
            shortcutsMap.put(behaviorName, behaviorShortcut);
        }
    }

    /**
     * Adding shortcut key with translation to HashMap if behavior key didn't exist
     *
     * @param behaviorName shortcut name
     * @param behaviorKey  shortcut key
     */
    public void addShortcutTranslation(final String behaviorName, final String behaviorKey) {
        if (!shortcutTranslationMap.containsKey(behaviorName)) {
            shortcutTranslationMap.put(behaviorName, behaviorKey);
        }
    }

    /**
     * Gets shortcut key
     * @param translation given translation
     * @return shortcut key
     */
    public String getShortcutKeyByTranslation(final String translation)
    {
        return shortcutTranslationMap.get(translation);
    }
}
