package com.horstmann.violet.product.diagram.property.text;

import java.awt.*;
import java.security.SecureRandom;
import java.util.*;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @date 14.01.2019
 */
public class TextContentCreator
{
	private final static String ERROR_MARKER = "!!";

	private final static String MARKER_LENGTH_LIMIT = "The length of the determinant can not exceed 4 characters";

	private final static String RGB_FORMAT = "(r, g, b)";

	private final static String RGBA_FORMAT = "(r, g, b, a)";

	private final static String PREPREPARED_FORMAT = "[1-18]";

	private final static String INCORRECT_FORMAT = "<INCORRECT_FORMAT>";

	private final static String PREPREPARED_COLOR_INCORRECT_NUMBER = "<INCORRECT_COLOR>, " + PREPREPARED_FORMAT;

	private final static String PREPREPARED_COLOR_INCORRECT_FORMAT = INCORRECT_FORMAT + " ," + ERROR_MARKER + PREPREPARED_FORMAT;

	private final static String RGB_COLOR_INCORRECT_FORMAT = INCORRECT_FORMAT + ERROR_MARKER + RGB_FORMAT;

	private final static String RGBA_COLOR_INCORRECT_FORMAT = INCORRECT_FORMAT + ERROR_MARKER + RGBA_FORMAT;



	private final static List<String> PREDEFINED_COLOR_PATTERN = Arrays.asList("[\\s]*[-]?[0-9]*\\.?,?[0-9]+");

	private final static List<String> RBG_COLOR_PATTERN = Arrays.asList("[\\s]*[(]+[-]?[0-9]*\\.?,?[0-9]+[\\s]*[,]+[\\s]*+[-]?[0-9]*\\.?,?[0-9]+[\\s]*[,]+[\\s]*[-]?[0-9]*\\.?,?[0-9]+[\\s]*[)]+",
		  "[\\s]*[(]+[-]?[0-9]*\\.?,?[0-9]+[\\s]*[,]+[\\s]*+[-]?[0-9]*\\.?,?[0-9]+[\\s]*[,]+[\\s]*[-]?[0-9]*\\.?,?[0-9]+[\\s]*[,]+[\\s]*[-]?[0-9]*\\.?,?[0-9]+[\\s]*[)]+");

	private final static List<String> UNKNOWN_PATTERN = Arrays.asList("[\\S]*");

	private final static List<String> ALPHABET_COLOR_PATTERN = Arrays.asList("[\\s]*[pP]", "[\\s]*[rR][aA][mM][bB][oO]", "[\\s]*[eE]", "[\\s]*[rR]");

	private final static List<String> ASSIGN_MARKER_PATTERN = Arrays.asList("[\\s]*[=][\\s]*[\\S]+");



	private final static String DECIMAL_NUMBER = "[-]?[0-9]*\\.?[0-9]+";

	private final static String DEFAULT_TEXT_FORMAT = "[\\s]*[dD]";

	private final static String BOLD_TEXT_FORMAT = "[\\s]*[bB]";

	private final static String ITALIZE_TEXT_FORMAT = "[\\s]*[iI]";

	private final static String UNDERLINE_TEXT_FORMAT = "[\\s]*[uU]";

	private final static String FONT_SIZE_TEXT_FORMAT = "[\\s]*[sS][\\s]*[-+][\\s]*" + DECIMAL_NUMBER;



	private final static Pattern TEXT_EFFECT_PATTERN = Pattern.compile("[\\s]*[dD]|[\\s]*[bB]|[\\s]*[iI]|[\\s]*[uU]|[\\s]*[sS][\\s]*[-+][\\s]*[-]?[0-9]*\\.?[0-9]+");

	private final static List<String> TEXT_EFFECT = Arrays.asList(DEFAULT_TEXT_FORMAT, BOLD_TEXT_FORMAT, ITALIZE_TEXT_FORMAT, UNDERLINE_TEXT_FORMAT, FONT_SIZE_TEXT_FORMAT);



	private final static Pattern objects = Pattern.compile(
		  "[\\s]*[pP]|[\\s]*[rR][aA][mM][bB][oO]|[\\s]*[eE]|[\\s]*[rR]|[\\s]*[(]+[-]?[0-9]*\\.?,?[0-9]+[\\s]*[,]+[\\s]*+[-]?[0-9]*\\.?,?[0-9]+[\\s]*[,]++[-]?[0-9]*\\.?,?[0-9]+[\\s]*[)]+|[\\s]*[-]?[0-9]*\\.?,?[0-9]+[\\s]*[(]+[-]?[0-9]*\\.?,?[0-9]+[\\s]*[,]+[\\s]*+[-]?[0-9]*\\.?,?[0-9]+[\\s]*[,]+[\\s]*[-]?[0-9]*\\.?,?[0-9]+[\\s]*[,]+[\\s]*[-]?[0-9]*\\.?,?[0-9]+[\\s]*[)]+|[\\s]*[=][\\s]*[\\S]+|[\\S]+");

	private final static List<List<String>> COLOR_COMPONENTS = Arrays.asList(ASSIGN_MARKER_PATTERN, PREDEFINED_COLOR_PATTERN, RBG_COLOR_PATTERN, ALPHABET_COLOR_PATTERN, UNKNOWN_PATTERN);



	private final static PrepreparedTextColor PREPREPARED_COLOR = new PrepreparedTextColor();

	private final static SecureRandom RND = new SecureRandom();

	private static String MARKER = "$";

	private static String REGEX_MARKER = "[$]";

	private static String REGEX_MARKER_NEGATION = "[^$]*";

	private static Pattern REGEX_MARKER_NEGATION_MARKER = Pattern.compile(REGEX_MARKER_NEGATION + REGEX_MARKER);

	private static Pattern MARKER_REGEX_MARKER_NEGATION = Pattern.compile(REGEX_MARKER + REGEX_MARKER_NEGATION);

	private static Color userDefaultColor = null;

	private static boolean ramboMode = false;

	private StringBuilder textBuilder;

	private boolean doesHaveCustomColor;

	private int colorChangeSpot;

	private int colorOccurrenceIndex;

	private Color color;

	private Color previousColor;

	transient private Color defaultColor;

	private MultiColoredText coloredText;

	TextContentCreator(Color defaultColor)
	{
		createContentStructure(defaultColor);
	}

	/**
	 * @return whether MARKER is present in the original text
	 */
	private static boolean hasColorContent(String originalText)
	{
		return originalText.contains(MARKER);
	}

	/**
	 *  @return pastel color
	 */
	private static Color generateRandomPastelColor()
	{
		float hue = RND.nextFloat();
		float saturation = (RND.nextInt(2000) + 1000) / 10000f;
		float luminance = 0.9f;
		return Color.getHSBColor(hue, saturation, luminance);
	}

	@Override
	public TextContentCreator clone()
	{
		try
		{
			return copy();
		} catch (CloneNotSupportedException e)
		{
			return null;
		}
	}

	protected TextContentCreator copy() throws CloneNotSupportedException
	{
		return new TextContentCreator(this.defaultColor);
	}

	/**
	 * sets text with its properties
	 *
	 * @see MultiColoredText.ColoredText#ColoredText(String, Color, ArrayList)
	 * @param text fragment of original text
	 * @param color
	 * @param effects text decorations
	 */
	private void setColoredTextPosition(String text, Color color, ArrayList<Integer> effects)
	{
		this.coloredText.add(this.colorOccurrenceIndex, text, color, effects);
	}

	private void createContentStructure(Color defaultColor)
	{
		this.textBuilder = new StringBuilder();
		this.color = defaultColor;
		this.previousColor = null;
		this.doesHaveCustomColor = false;
		this.defaultColor = defaultColor;
	}

	/**
	@return color of text fragment
	 */
	public Color getTextColor()
	{
		if (userDefaultColor != null)
		{
			if (ramboMode)
			{
				return generateRandomPastelColor();
			}
			else
			{
				this.doesHaveCustomColor = true;
				return userDefaultColor;
			}
		}
		return this.color;
	}

	/**
	 * @return parsed text with its properties
	 */
	public MultiColoredText getTextColors()
	{
		return this.coloredText;
	}

	/**
	 * @return parsed content
	 */
	public String getTextContent()
	{
		return this.textBuilder.toString();
	}

	/**
	 * @return parsed content size
	 */
	private int getTextContentLength()
	{
		return getTextContent().length();
	}

	/**
	 *  restores values of content structure before processing original text
	 */
	private void restoreContentStructure()
	{
		this.textBuilder = new StringBuilder();
		this.doesHaveCustomColor = false;
		this.coloredText = new MultiColoredText();
		this.colorChangeSpot = 0;
		this.colorOccurrenceIndex = 0;
	}

	/**
	 * acquires text properties
	 * @param originalText input
	 */

	public void acquireTextProperties(String originalText)
	{
		if (!hasColorContent(originalText) && userDefaultColor == null)
		{
			this.textBuilder = new StringBuilder(originalText);
			return;
		}

		restoreContentStructure();

		Matcher matcherOfMarker = REGEX_MARKER_NEGATION_MARKER.matcher(originalText);

		LinkedList<Integer> markerOccurrencePosition = new LinkedList<>();

		LinkedList<String> textWithoutMarkerAndDecorations = new LinkedList<>();

		LinkedList<ArrayList<Integer>> textDecorations = new LinkedList<>();

		String helper;

		int positionToParseColor = 0;

		if (matcherOfMarker.find())
		{
			markerOccurrencePosition.add(-1);
			helper = originalText.substring(0, matcherOfMarker.end() - 1);
			positionToParseColor = helper.length();
			textWithoutMarkerAndDecorations.add(helper);
		}
		else
		{
			markerOccurrencePosition.add(0);
			textWithoutMarkerAndDecorations.add(originalText);
		}
		textDecorations.add(new ArrayList<>(6));

		Matcher matcherOfTextSpecialEffects = MARKER_REGEX_MARKER_NEGATION.matcher(originalText);

		boolean omitTheMarkerAfterItsDoubleOccurence = false;

		while (matcherOfTextSpecialEffects.find())
		{
			String matchedTextSpecialEffect = matcherOfTextSpecialEffects.group();
			if (matchedTextSpecialEffect.length() == MARKER.length())
			{
				if (textWithoutMarkerAndDecorations.isEmpty())
				{
					helper = MARKER;
					positionToParseColor += helper.length();
					textWithoutMarkerAndDecorations.add(helper);
				}
				else
				{
					helper = textWithoutMarkerAndDecorations.getLast() + MARKER;
					positionToParseColor += MARKER.length();
					textWithoutMarkerAndDecorations.set(textWithoutMarkerAndDecorations.size() - 1, helper);
				}
				omitTheMarkerAfterItsDoubleOccurence = true;
			}
			else
			{
				textDecorations.add(new ArrayList<>(6));
				matchedTextSpecialEffect = matchedTextSpecialEffect.replaceAll(REGEX_MARKER, "");
				if (!omitTheMarkerAfterItsDoubleOccurence)
				{
					int comparingIndex = 0;
					Matcher matcherEffect = TEXT_EFFECT_PATTERN.matcher(matchedTextSpecialEffect);
					while (matcherEffect.find())
					{
  outsideTextSpecialEffectMatchingLoop:
						for (ListIterator<String> patternGroup = TEXT_EFFECT.listIterator(); patternGroup.hasNext(); )
						{
							String matcherFound = matcherEffect.group();
							if (matcherFound.matches(patternGroup.next()))
							{
								try
								{
									if (matcherEffect.start() != comparingIndex)
									{
										continue outsideTextSpecialEffectMatchingLoop;
									}
									if (!textDecorations.getLast().contains(patternGroup.previousIndex()))
									{
										textDecorations.getLast().add(patternGroup.previousIndex());
										if (patternGroup.previousIndex() == 4)
										{
											try
											{
												String textSize = matcherFound.replace("\\s", "").substring(2, matcherFound.length());
												textDecorations.getLast().add(-(new Integer(textSize)));
											} catch (IllegalArgumentException iae)
											{
												// ignore and proceed further (it will not affect font size)
											}
										}
										matchedTextSpecialEffect = matchedTextSpecialEffect.substring(matcherFound.length(), matchedTextSpecialEffect.length());
										comparingIndex += matcherFound.length();
										matcherEffect.reset();
									}
								} catch (IllegalArgumentException ioe)
								{
									continue outsideTextSpecialEffectMatchingLoop;
								} finally
								{
									break outsideTextSpecialEffectMatchingLoop;
								}
							}
							continue outsideTextSpecialEffectMatchingLoop;
						}
					}
					markerOccurrencePosition.add(positionToParseColor);
				}
				else
				{
					omitTheMarkerAfterItsDoubleOccurence = false;
				}
				helper = matchedTextSpecialEffect;
				positionToParseColor += matchedTextSpecialEffect.length();
				textWithoutMarkerAndDecorations.add(helper);
			}
		}

		String textToParseColors = textWithoutMarkerAndDecorations.stream().reduce("", String::concat);

		markerOccurrencePosition.add(textToParseColors.length());
		markerOccurrencePosition.remove(0);

		textDecorations.add(textDecorations.getLast());
		textDecorations.remove(0);

		int occurrenceOfLastColor = 0;

		Iterator<ArrayList<Integer>> textSpecialEffectIterator = textDecorations.iterator();

		ArrayList<Integer> textDecoration = textDecorations.getFirst();
  outsideColorMatchingLoop:
		for (Integer occurrenceOfColor : markerOccurrencePosition)
		{
			Matcher matcherOfColors = objects.matcher(textToParseColors.substring(this.colorOccurrenceIndex, occurrenceOfColor));
			this.colorOccurrenceIndex = occurrenceOfColor;

			while (matcherOfColors.find())
			{
				for (ListIterator<java.util.List<String>> patternGroup = COLOR_COMPONENTS.listIterator(); patternGroup.hasNext(); )
				{
					for (ListIterator<String> patternSubGroup = patternGroup.next().listIterator(); patternSubGroup.hasNext(); )
					{
						if (matcherOfColors.group().matches(patternSubGroup.next()) && matcherOfColors.start() == 0)
						{
							try
							{
								int component_group = patternGroup.previousIndex();
								int component_subgroup = patternSubGroup.previousIndex();
								String foundObject = matcherOfColors.group();

								// adds color
								parseTextProperties(component_group, component_subgroup, foundObject, textDecoration);

								// adds textWithoutMarkerAndDecoration reduced by color indicator into text content
								this.textBuilder.append(textToParseColors.substring(occurrenceOfLastColor + matcherOfColors.end(), occurrenceOfColor));

								// acquire next text decoration
								textDecoration = textSpecialEffectIterator.next();

								// declare position of last index found object
								occurrenceOfLastColor = occurrenceOfColor;

							} catch (IllegalArgumentException ioe)
							{
								throw new IllegalStateException();
							} finally
							{
								continue outsideColorMatchingLoop;
							}
						}
					}
				}
			}
		}
		String lastTextFragment = textToParseColors.substring(this.colorOccurrenceIndex, textToParseColors.length());

		this.textBuilder.append(lastTextFragment);

		this.colorOccurrenceIndex = Integer.MAX_VALUE;

		setColoredTextPosition(getTextContent().substring(this.colorChangeSpot, getTextContentLength()), getTextColor(), textDecorations.getLast());
	}

	/**
	 * sets the index in which the color change occurred
	 */
	private void setColorChangeSpot()
	{
		int textContentLength = getTextContentLength();
		this.colorChangeSpot = textContentLength == 0 ? 0 : textContentLength - 1;
	}

	/**
	 * sets the user default color
	 */
	private void setUserDefaultColor(ArrayList<Integer> textDecorations)
	{
		if (textDecorations.contains(0))
		{
			userDefaultColor = color;
		}
	}

	/**
	 * sets default color
	 */
	private void setDefaultColor(ArrayList<Integer> textDecorations)
	{
		setUserDefaultColor(textDecorations);
		setColoredTextPosition(getTextContent().substring(this.colorChangeSpot, getTextContentLength()), this.color, textDecorations);
		setColorChangeSpot();
		this.color = this.defaultColor;
	}

	/**
	 * sets color
	 * @param color last color
	 */
	private void setColor(Color color, ArrayList<Integer> textDecorations)
	{
		setUserDefaultColor(textDecorations);
		textBuilder.append(" ");
		if (!this.doesHaveCustomColor)
		{
			setColoredTextPosition(getTextContent(), color, textDecorations);
			setColorChangeSpot();
			this.doesHaveCustomColor = true;

			if (previousColor == null || color != previousColor)
			{
				this.previousColor = color;
			}
			this.color = color;
		}
		else
		{
			setColoredTextPosition(getTextContent().substring(this.colorChangeSpot, getTextContentLength()), this.previousColor, textDecorations);
			setColorChangeSpot();
			if (color != previousColor)
			{
				this.previousColor = color;
			}
			this.color = color;
		}
	}

	/**
	 * converts text into error message
	 *
	 * @param text message content
	 */
	private String errorMessage(String text)
	{
		return ERROR_MARKER + text + ERROR_MARKER;
	}

	/**
	 * changes the marker sign
	 * @param text potential marker
	 */
	private void parseNewMarkerSign(String text)
	{
		String newMarker = text.replaceAll("\\s", "").substring(1, text.length());
		if (MARKER.equals(newMarker) || newMarker.equals(""))
		{
			return;
		}
		if (newMarker.length() > 5)
		{
			textBuilder.append(errorMessage(MARKER_LENGTH_LIMIT));
		}
		else
		{
			MARKER = newMarker;
			REGEX_MARKER = "";
			REGEX_MARKER_NEGATION = "";
			for (int i = 0, n = newMarker.length(); i < n; i++)
			{
				REGEX_MARKER += "[" + newMarker.charAt(i) + "]";
				REGEX_MARKER_NEGATION += "[^" + newMarker.charAt(i) + "]";
			}
			REGEX_MARKER_NEGATION += "*";
			REGEX_MARKER_NEGATION_MARKER = Pattern.compile(REGEX_MARKER_NEGATION + REGEX_MARKER);
			MARKER_REGEX_MARKER_NEGATION = Pattern.compile(REGEX_MARKER + REGEX_MARKER_NEGATION);
		}
	}

	/**
	 * switching parsing process by group
	 */
	private void parseTextProperties(int group, int subGroup, String findObjects, ArrayList<Integer> textDecorations)
	{

		switch (group)
		{
			case 0:
				parseNewMarkerSign(findObjects);
				break;
			case 1:
				parseColorFromPrepreparedColors(findObjects, textDecorations);
				break;
			case 2:
				parseColorFromRGB(subGroup, findObjects, textDecorations);
				break;
			case 3:
				parseColorFromAlphabetCharacters(subGroup, textDecorations);
				break;
			case 4:
				parseUnknownColorDetection(findObjects);
				break;
		}
	}

	/**
	 * switching parsing process by AlphabetCharacter subgroup
	 */
	private void parseColorFromAlphabetCharacters(int subGroup, ArrayList<Integer> textDecorations)
	{
		switch (subGroup)
		{
			case 0:
				setColor(this.previousColor == null ? this.color : this.previousColor, textDecorations);
				break;
			case 1:
				setColor(this.defaultColor, textDecorations);
				userDefaultColor = this.color;
				ramboMode = true;
			case 2:
				setColor(this.defaultColor, textDecorations);
				userDefaultColor = null;
				ramboMode = false;
			case 3:
				setColor(generateRandomPastelColor(), textDecorations);
		}
	}

	/**
	 * sets color from RGB
	 */
	private void parseColorFromRGB(int subGroup, String text, ArrayList<Integer> textDecorations)
	{
		try
		{
			text = text.replaceAll("\\)", "").replaceAll("\\(", "");
			Float[] RGB = Arrays.stream(text.split(",")).map(Float::parseFloat).toArray(Float[]::new);
			Color RGBColor;
			if (subGroup == 0)
			{
				RGBColor = new Color(RGB[0], RGB[1], RGB[2]);
			}
			else
			{
				RGBColor = new Color(RGB[0], RGB[1], RGB[2], RGB[3]);
			}
			setColor(RGBColor, textDecorations);
		} catch (IllegalArgumentException ioe)
		{
			String messsage;
			messsage = ERROR_MARKER;
			if (subGroup == 0)
			{
				messsage += RGB_COLOR_INCORRECT_FORMAT;
			}
			else
			{
				messsage += RGBA_COLOR_INCORRECT_FORMAT;
			}
			setDefaultColor(textDecorations);
			this.textBuilder.append(messsage);
		}
	}

	/**
	 * sets only text content without color when color detection is unknown
	 */
	private void parseUnknownColorDetection(String text)
	{
		if (!text.contains(MARKER))
		{
			this.textBuilder.append(text);
		}
	}

	/**
	 * sets color from Preprepared colors
	 */
	private void parseColorFromPrepreparedColors(String text, ArrayList<Integer> textDecorations)
	{
		String errorMessage = ERROR_MARKER;
		try
		{
			Color prepreparedColor = PREPREPARED_COLOR.getPrepreparedColor(Integer.parseInt(text.replaceAll("\\s", "")));
			if (prepreparedColor != null)
			{
				setColor(prepreparedColor, textDecorations);
			}
			else
			{
				setDefaultColor(textDecorations);
				this.textBuilder.append(errorMessage).append(errorMessage(PREPREPARED_COLOR_INCORRECT_NUMBER));
			}
		} catch (IllegalArgumentException ioe)
		{
			setDefaultColor(textDecorations);
			this.textBuilder.append(errorMessage).append(errorMessage(PREPREPARED_COLOR_INCORRECT_FORMAT));
		}
	}
}

