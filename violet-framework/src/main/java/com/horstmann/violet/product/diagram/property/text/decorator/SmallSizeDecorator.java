package com.horstmann.violet.product.diagram.property.text.decorator;

import com.horstmann.violet.framework.userpreferences.PreferencesConstant;
import com.horstmann.violet.framework.userpreferences.PreferencesServiceFactory;

/**
 * This class decreases text
 *
 * @author Adrian Bobrowski <adrian071993@gmail.com>
 * @date 16.12.2015
 */
public class SmallSizeDecorator extends OneLineTextDecorator
{
    private static final long serialVersionUID = 1;
    private int decreases;
    
    public SmallSizeDecorator(OneLineText decoratedOneLineString)
    {
        this(decoratedOneLineString,1);
    }
    public SmallSizeDecorator(OneLineText decoratedOneLineString, int decreases)
    {
        super(decoratedOneLineString);
        if(0>=decreases)
        {
            throw new IllegalArgumentException("decreases have to positive number");
        }

        String fontSizeSetting = PreferencesServiceFactory.getInstance().get(PreferencesConstant.FONT_SIZE, "0");
        this.decreases = new Integer(fontSizeSetting) + decreases;
    }

    /**
     * @see OneLineText#toDisplay()
     */
    @Override
    public String toDisplay()
    {
        return "<font size=-" + decreases + ">" + decoratedOneLineString.toDisplay() + "</font>";
    }
}
