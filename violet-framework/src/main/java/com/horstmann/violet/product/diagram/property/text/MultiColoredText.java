package com.horstmann.violet.product.diagram.property.text;

import java.awt.*;
import java.util.*;

/**
 * @date 14.01.2019
 *
 * Container for the color data and iterator with relation in order to elements of original text
 */
public class MultiColoredText
{
	private SortedIndexedLinkedHashMap<Integer, ColoredText> context;
	private ColorIterator iterator;

	public MultiColoredText()
	{
		this.context = new SortedIndexedLinkedHashMap<>();
	}

	private SortedIndexedLinkedHashMap<Integer, ColoredText> getContext()
	{
		return this.context;
	}

	private ColorIterator getIterator()
	{
		return iterator;
	}
	/**
	 * @return no colors inside context
	 */
	public boolean isEmpty()
	{
		return 0 == this.context.size();
	}

	/**
	 * Adding information about a single color to the context
	 *
	 * @param position - position in relation to the origin text
	 * @param text - context that will be colored
	 * @param color
	 */
	public void add(Integer position, String text, Color color, ArrayList<Integer> effects)
	{
		this.context.put(position, new ColoredText(text, color, effects));
	}

	public void iterate()
	{
		this.iterator = new ColorIterator();
	}

	public boolean hasNext()
	{
		return getIterator().hasNext();
	}

	public Color getColor()
	{
		return getIterator().getColor();
	}

	public String getText()
	{
		return getIterator().getText();
	}

	public void next()
	{
		getIterator().next();
	}

	public Integer getFontSize()
	{
		return getIterator().getFontSize();
	}

	public boolean doesItContainBoldEffect()
	{
		return this.getIterator().doesItContainBoldEffect();
	}

	public boolean doesItContainItalizeEffect()
	{
		return this.getIterator().doesItContainItalizeEffect();
	}

	public boolean doesItContainUnderlineEffect()
	{
		return this.getIterator().doesItContainUnderlineEffect();
	}

	/**
	 * Class for secure iterate through colors of the original text
	 * {@param coloredText} stores the current item
	 */
	public class ColorIterator
	{

		private ColoredText coloredText;

		Iterator<ColoredText> coloredTextIterator;

		private ColorIterator()
		{
			this.coloredTextIterator = getContext().iterator();
		}

		/**
		 * @return true if contains next element
		 */

		public boolean hasNext()
		{
			return this.coloredTextIterator.hasNext();
		}

		/**
		 * @code move to the next element
		 */
		public void next()
		{
			this.coloredText = coloredTextIterator.next();
		}

		public Color getColor()
		{
			return this.coloredText.getColor();
		}

		public String getText()
		{
			return this.coloredText.getText();
		}

		public Integer getFontSize()
		{
			return this.coloredText.getFontSize();
		}

		public boolean doesItContainBoldEffect()
		{
			return this.coloredText.doesItContainBoldEffect();
		}

		public boolean doesItContainItalizeEffect()
		{
			return this.coloredText.doesItContainItalizeEffect();
		}

		public boolean doesItContainUnderlineEffect()
		{
			return this.coloredText.doesItContainUnderlineEffect();
		}
	}


	/**
	 * Class for restoring the order of elements to the original text
	 *
	 * @param <POSITION> relation to parsing function
	 * @param <ELEMENT> element
	 */

	private class SortedIndexedLinkedHashMap<POSITION, ELEMENT> extends LinkedHashMap<POSITION, ELEMENT>
	{
		/**
		 * @param iteratorIndex bridge between {@param <POSITION>} and {@param <ELEMENT>}
		 */
		transient Map<Integer, POSITION> position;

		int currentIndex;

		private SortedIndexedLinkedHashMap()
		{
			position = new HashMap<>();
			currentIndex = 0;
		}

		public ELEMENT put(POSITION key, ELEMENT val)
		{
			super.put(key, val);
			position.put(currentIndex++, key);
			return val;
		}

		public ELEMENT get(int i)
		{
			return super.get(position.get(i));
		}

		public Integer getSize()
		{
			return this.position.size();
		}

		/**
		 * @return the context in the right order in relation to the origin text
		 *
		 * @code before using {@link Iterator< ELEMENT >} check {@link MultiColoredText#isEmpty()}
		 * @code assumed that the {@link Iterator< ELEMENT >#hasNext()}  will be called before {@link Iterator< ELEMENT >#next()}
		 */

		public Iterator<ELEMENT> iterator()
		{
			getContext().sort();
			return new Iterator<ELEMENT>()
			{

				int iteratorIndex = 0;

				@Override
				public boolean hasNext()
				{
					return iteratorIndex < getSize();
				}

				@Override
				@SuppressWarnings ("NoSuchElementException")
				public ELEMENT next()
				{
					if (!this.hasNext())
					{
						throw new NoSuchElementException();
					}
					return get(iteratorIndex++);
				}

				@Override
				public void remove()
				{
					throw new UnsupportedOperationException();
				}
			};
		}


		/**
		 * Generic function to sort Map in Java by reverse ordering of its keys
		 */
		private <KEY extends Comparable, ELEMENT> Map<KEY, ELEMENT> sortByKeys(Map<KEY, ELEMENT> map)
		{
			Map<KEY, ELEMENT> treeMap = new TreeMap<>(new Comparator<KEY>()
			{

				@Override
				public int compare(KEY a, KEY b)
				{
					return a.compareTo(b);
				}
			});

			Map<KEY, ELEMENT> sortedMap = new HashMap<>();

			Integer counter = 0;

			treeMap.putAll(map);

			for (Map.Entry<KEY, ELEMENT> entry : map.entrySet())
			{
				sortedMap.put((KEY) counter++, entry.getValue());
			}

			return sortedMap;
		}

		private void  sort()
		{
			this.position = sortByKeys(this.position);
		}
	}

	/**
	 * Class for storing data about colors. (text with chosen properties)
	 */
	public class ColoredText
	{
		private String text;

		private Color color;

		private ArrayList<Integer> effects;

		ColoredText(String text, Color color, ArrayList<Integer> effects)
		{
			this.text = text;
			this.color = color;
			this.effects = effects;
		}

		private String getText()
		{
			return this.text;
		}

		private Color getColor()
		{
			return this.color;
		}


		private boolean doesItContainBoldEffect()
		{
			return containsOption(1);
		}

		private boolean doesItContainItalizeEffect()
		{
			return containsOption(2);
		}

		private boolean doesItContainUnderlineEffect()
		{
			return containsOption(3);
		}

		/**
		 * @return if exists returns the size of the font
		 */
		private Integer getFontSize(){
			if(containsOption(4)) {
				for(Integer element : this.effects)
				{
					if(element < 0) {
						return (- (element));

					}
				}
			}
			return null;
		}

		/**
		 *
		 * @param option value of searched element
		 * @return is the element in the effects collection
		 */

		private boolean containsOption(Integer option)
		{
			if(this.effects == null)
			{
				return false;
			}

			if (this.effects.contains(option))
			{
				return true;
			}
			return false;
		}
	}
}
