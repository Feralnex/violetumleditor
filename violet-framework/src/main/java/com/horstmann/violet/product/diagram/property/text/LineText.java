package com.horstmann.violet.product.diagram.property.text;

import com.horstmann.violet.product.diagram.property.text.decorator.OneLineText;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class is a container for text
 *
 * @author Adrian Bobrowski <adrian071993@gmail.com>
 * @author MankindEnemy (Maniek)
 * @date 16.12.2015
 * @date 14.01.2019
 */
public abstract class LineText implements Serializable, Cloneable, EditableText
{
	public static final Converter DEFAULT_CONVERTER = new Converter()
	{

		@Override
		public OneLineText toLineString(String text)
		{
			return new OneLineText(text);
		}
	};

	public static final int LEFT = SwingConstants.LEFT;

	public static final int CENTER = SwingConstants.CENTER;

	public static final int RIGHT = SwingConstants.RIGHT;

	public static final Pattern EXCEPTION_IN_PROCESSING_TEXT_COLOR_PATTERN = Pattern.compile("[<][c](.*?)[<][/][c][e][n][t][e][r][>]");

	public static final Pattern COLOR_PROCESSING_PATTERN = Pattern.compile("[<][f][o][n][t](.*?)[>]");

	public static final String FONT_SIZE_PROCESSING_PATTERN = "[s][i][z][e][=][+]?[0-9]+";

	public static final Pattern TEXT_GROUP_PATTERN = Pattern.compile("[<][f][o][n][t](.*?)[>](.*?)[/][f][o][n][t][>]");

	protected transient Converter converter;

	private transient Rectangle2D bounds;

	private transient List<ChangeListener> changeListeners;

	private transient JLabel label;

	public LineText()
	{
		this(DEFAULT_CONVERTER);
	}

	public LineText(Converter converter)
	{
		this.converter = converter;
	}

	protected LineText(LineText lineText) throws CloneNotSupportedException
	{
		getLabel().setHorizontalAlignment(lineText.getLabel().getHorizontalAlignment());
		getLabel().setVerticalAlignment(lineText.getLabel().getVerticalAlignment());
		getLabel().setForeground(lineText.getLabel().getForeground());
		getLabel().setBorder(lineText.getLabel().getBorder());
		getLabel().setText(lineText.getLabel().getText());
		converter = lineText.converter;
	}

	/**
	 * set text to label
	 *
	 * @return RGB color value in hex notation
	 *
	 */
	private static String setHexColorValue(Color color)
	{
		return "#" + Integer.toHexString(color.getRGB()).substring(2);
	}

	public final void reconstruction()
	{
		this.reconstruction(DEFAULT_CONVERTER);
	}

	public void reconstruction(Converter converter)
	{
		this.converter = converter;
	}

	@Override
	public LineText clone()
	{
		try
		{
			return copy();
		} catch (CloneNotSupportedException e)
		{
			return null;
		}
	}

	protected LineText copy() throws CloneNotSupportedException
	{
		return null;
	}

	/**
	 * set converter
	 *
	 * @param converter
	 * @see Converter
	 */
	public final void setConverter(Converter converter)
	{
		this.converter = converter;
	}

	/**
	 * @return bounds of text in label
	 */
	public final Rectangle2D getBounds()
	{
		if (null == bounds)
		{
			bounds = new Rectangle2D.Double(0, 0, 0, 0);
		}
		return bounds;
	}

	public final void setText(EditableText text)
	{
		setText(text.toEdit());
	}

	/**
	 * @return label color
	 */
	public final Color getTextColor()
	{
		return getLabel().getForeground();
	}

	/**
	 * sets label color
	 *
	 * @param color
	 */
	public final void setTextColor(Color color)
	{
		getLabel().setForeground(color);
	}

	/**
	 * sets label padding
	 *
	 * @param padding
	 */
	public final void setPadding(int padding)
	{
		setPadding(padding, padding);
	}

	/**
	 * sets label padding
	 *
	 * @param vertical
	 * @param horizontal
	 */
	public final void setPadding(int vertical, int horizontal)
	{
		setPadding(vertical, horizontal, vertical, horizontal);
	}

	/**
	 * sets label padding
	 *
	 * @param top
	 * @param left
	 * @param bottom
	 * @param right
	 */
	public final void setPadding(int top, int left, int bottom, int right)
	{
		getLabel().setBorder(new EmptyBorder(top, left, bottom, right));
		refresh();
	}

	/**
	 * @return label alignments
	 */
	public final int getAlignment()
	{
		return getLabel().getHorizontalAlignment();
	}

	/**
	 * sets label alignments
	 *
	 * @param flag LEFT CENTER RIGHT
	 */
	public final void setAlignment(int flag)
	{
		getLabel().setHorizontalAlignment(flag);
		refresh();
	}

	/**
	 * Draws text with specific size
	 *
	 * @param graphics
	 * @param rect
	 */
	public final void draw(Graphics2D graphics, Rectangle2D rect)
	{
		getLabel().setBounds(0, 0, (int) rect.getWidth(), (int) rect.getHeight());
		draw(graphics, new Point2D.Double(rect.getX(), rect.getY()));
	}

	/**
	 * Draws text shifted by offset
	 *
	 * @param graphics
	 * @param point
	 */
	public final void draw(Graphics2D graphics, Point2D point)
	{
		graphics.translate(point.getX(), point.getY());
		getLabel().paint(graphics);
		graphics.translate(-point.getX(), -point.getY());
	}

	/**
	 * Draws text
	 *
	 * @param graphics
	 */
	public final void draw(Graphics2D graphics)
	{
		getLabel().setBounds(0, 0, (int) getBounds().getWidth(), (int) getBounds().getHeight());
		draw(graphics, new Point2D.Double(0, 0));
	}

	/**
	 * add listener
	 *
	 * @param changeListener
	 */
	public final void addChangeListener(ChangeListener changeListener)
	{
		if (null == changeListener)
		{
			throw new NullPointerException("ChangeListener can't be null");
		}
		getChangeListeners().add(changeListener);
	}

	/**
	 * notify all listeners that there is a change
	 */
	protected final void notifyAboutChange()
	{
		for (ChangeListener changeListener : getChangeListeners())
		{
			changeListener.onChange();
		}
	}

	/**
	 * adding text decoration effects
	 *
	 * @param text parsed text with color effect and font size
	 * @param colors Data for the construction of the text
	 * @return labelText including decorations
	 */

	private String setLabelTextWithDecorations(String text, MultiColoredText colors)
	{
		Matcher matcherOfGroups = TEXT_GROUP_PATTERN.matcher(text);

		String textWithDecorations = "";

		colors.iterate();


		while (matcherOfGroups.find())
		{

			colors.next();

			String head = "";
			String tail = "";

			if(colors.doesItContainBoldEffect()) {
				head += "<b>" + head;
				tail += tail + "</b>";
			}

			if(colors.doesItContainItalizeEffect()) {
				head += "<i>" + head;
				tail += tail + "</i>";
			}

			if(colors.doesItContainUnderlineEffect()) {
				head += "<u>" + head;
				tail += tail + "</u>";
			}

			textWithDecorations += head + matcherOfGroups.group() + tail;
		}
		return  textWithDecorations;
	}

	/**
	 * adding font size into text
	 *
	 * @param text parsed text with color effect
	 * @param colors Data for the construction of the text
	 * @return labelText including  FontSize, decoration
	 */

	private String setLabelTextWithFontSize(String text, MultiColoredText colors)
	{
		Matcher matcherOfGroups = TEXT_GROUP_PATTERN.matcher(text);

		String resizedText = "";

		colors.iterate();

		while(matcherOfGroups.find()) {

			colors.next();

			String foundGroup = matcherOfGroups.group();

			Integer fontSize = colors.getFontSize();

			if (fontSize != null)
			{
				if (fontSize < 11)
				{
					if (foundGroup.matches(FONT_SIZE_PROCESSING_PATTERN))
					{
						resizedText += foundGroup.replaceAll("[s][i][z][e][=][+]?[\\d]+", "size=" + fontSize);
					}
					else
					{
						resizedText += foundGroup.replace("<font", "<font size=" + fontSize);
					}
				}
				else
				{
					resizedText += foundGroup;
				}
			}
			else
			{
				resizedText += foundGroup;
			}
		}
		return setLabelTextWithDecorations(resizedText, colors);
	}

	/**
	 * adding colors to the text
	 *
	 * @param text original Text
	 * @param colors data for the construction of the text
	 * @return labelText including Color, FontSize, Decorations
	 */

	private String setLabelTextWithColors(String text, MultiColoredText colors)
	{
		String coloredText = "";

		Matcher matcherOfColors = COLOR_PROCESSING_PATTERN.matcher(text);

		colors.iterate();

		boolean HTMLsyntaxFound = matcherOfColors.find();

		while (colors.hasNext())
		{
			colors.next();

			Color textColor = colors.getColor();

			String textContent = replaceForUnification(" " + colors.getText().replaceAll("\n", "<br>"));

			String hexColorValue = setHexColorValue(textColor);

			if (HTMLsyntaxFound)
			{
				coloredText += matcherOfColors.group().replace("<font", "<font color=" + hexColorValue) + textContent + "</font>";
			}
			else
			{
				coloredText += "<font color=" + hexColorValue + " >" + textContent + "</font>";
			}
		}
		return setLabelTextWithFontSize(coloredText, colors);
	}

	/**
	 * skips the key text fragment
	 *
	 * @param text original Text
	 * @return skipped text fragment
	 */

	private String setLabelWithoutTextEffectOnSkippedKeyFragment(String text)
	{
		Matcher matcherOfException = EXCEPTION_IN_PROCESSING_TEXT_COLOR_PATTERN.matcher(text);

		String skippedTextFragment;

		// find an exception skipped in overall processing (<<enumerable>> etc. )

		if (matcherOfException.find())
		{
			skippedTextFragment = matcherOfException.group();
		}
		else
		{
			skippedTextFragment = "";
		}

		return skippedTextFragment;
	}

	/**
	 * adding skipped key text fragment
	 *
	 * @param text original Text
	 * @param colors data for the construction of the text
	 * @return labelText including Color, FontSize, Decorations
	 */

	private String setLabelTextWithTextEffects(String text, MultiColoredText colors)
	{
		String skippedKeyFragment = setLabelWithoutTextEffectOnSkippedKeyFragment(text);
		String textWithoutSkippedFragment = text.replaceFirst(skippedKeyFragment, "");
		return skippedKeyFragment + setLabelTextWithColors(textWithoutSkippedFragment, colors);
	}

	/**
	 * set text to label with proper HTML color tags
	 *
	 * @param text
	 * @author Maniek
	 */
	protected final void setLabelText(String text, MultiColoredText colors)
	{
		if (text.isEmpty())
		{
			getLabel().setText("");
		}
		else
		{
			getLabel().setText("<html>" + setLabelTextWithTextEffects(text, colors) + "<html>");
		}

		refresh();
	}

	/**
	 * Replace all special characters for unification text.
	 *
	 * @param sentence input string
	 * @return the replaced string
	 *
	 * @author Maniek
	 */
	protected final String replaceForUnification(String sentence)
	{
		return sentence.replace("<<", "«").replace(">>", "»");
	}


	/**
	 * set text to label without color modification
	 *
	 * @param text
	 * @author Maniek
	 */
	protected final void setLabelText(String text)
	{
		if (text.isEmpty())
		{
			getLabel().setText("");
		}
		else
		{
			getLabel().setText("<html>" + text + "<html>");
		}

		refresh();
	}

	/**
	 * Recalculate preferred size for text
	 */
	private void refresh()
	{
		if (getLabel().getText().isEmpty())
		{
			this.bounds = new Rectangle2D.Double(0, 0, 0, 0);
		}
		else
		{
			Dimension dimension = getLabel().getPreferredSize();
			this.bounds = new Rectangle2D.Double(0, 0, dimension.getWidth(), dimension.getHeight());
		}
	}

	/**
	 * @return label
	 *
	 * @see JLabel
	 */
	private JLabel getLabel()
	{
		if (null == label || null == label.getText())
		{
			label = new JLabel("");
		}
		return label;
	}

	/**
	 * @return list of listeners
	 */
	private List<ChangeListener> getChangeListeners()
	{
		if (null == changeListeners)
		{
			changeListeners = new ArrayList<ChangeListener>();
		}
		return changeListeners;
	}

	public interface Converter
	{
		/**
		 * converts plain text to one that may contain decorators
		 *
		 * @param text
		 * @return
		 */
		OneLineText toLineString(String text);
	}

	public interface ChangeListener
	{
		/**
		 * This function is called when the text is changed
		 */
		void onChange();
	}

}
