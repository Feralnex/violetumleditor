package com.horstmann.violet.product.diagram.property.text.decorator;

import com.horstmann.violet.framework.userpreferences.PreferencesConstant;
import com.horstmann.violet.framework.userpreferences.PreferencesServiceFactory;

/**
 * This class increases text
 *
 * @author Adrian Bobrowski <adrian071993@gmail.com>
 * @date 16.12.2015
 */
public class LargeSizeDecorator extends OneLineTextDecorator
{
    private static final long serialVersionUID = 1;
    public LargeSizeDecorator(OneLineText decoratedOneLineString)
    {
        this(decoratedOneLineString,1);
    }
    public LargeSizeDecorator(OneLineText decoratedOneLineString, int increase) {
        super(decoratedOneLineString);

        if(0>=increase)
        {
            throw new IllegalArgumentException("increase have to positive number");
        }
        String fontSizeSetting = PreferencesServiceFactory.getInstance().get(PreferencesConstant.FONT_SIZE, "1");
        this.increase = new Integer(fontSizeSetting) + increase;
    }

    /**
     * @see OneLineText#toDisplay()
     */
    @Override
    public String toDisplay() {
        return "<font size=+" + increase + ">" + decoratedOneLineString.toDisplay() + "</font>";
    }

    private int increase;
}
