package com.horstmann.violet.product.diagram.property.text.decorator;

import com.horstmann.violet.framework.userpreferences.IUserPreferencesDao;
import com.horstmann.violet.framework.userpreferences.PreferencesServiceFactory;

/**
 * This ...
 *
 * @author Adrian Bobrowski <adrian071993@gmail.com>
 * @date 12.12.2015
 */
public class OneLineTextDecorator extends OneLineText
{
    private static final long serialVersionUID = 1;
    protected OneLineText decoratedOneLineString;
    protected transient IUserPreferencesDao preferencesService;

    public OneLineTextDecorator(OneLineText decoratedOneLineString)
    {
        this.decoratedOneLineString = decoratedOneLineString;
        preferencesService = PreferencesServiceFactory.getInstance();
    }

    @Override
    public OneLineTextDecorator clone()
    {
        return new OneLineTextDecorator(this.decoratedOneLineString.clone());
    }

    /**
     * @see OneLineText#toDisplay()
     */
    @Override
    public String toDisplay()
    {
        return decoratedOneLineString.toDisplay();
    }
    /**
     * @see OneLineText#toEdit()
     */
    @Override
    public String toEdit()
    {
        return decoratedOneLineString.toEdit();
    }
}
