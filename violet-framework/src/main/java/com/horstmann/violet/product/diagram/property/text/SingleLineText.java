package com.horstmann.violet.product.diagram.property.text;

import com.horstmann.violet.product.diagram.property.text.decorator.OneLineText;

/**
 * This class is a container for a single line of text
 */
public class SingleLineText extends LineText
{
	/**
	 * This class is a container for a single line of text
	 *
	 * @code OneLineText was divided into two separate parameters
	 *
	 * @param oneLineStringDisplay text that is displayed with colors (processed)
	 * @param oneLineStringEdit text that is displayed in editable box (processed)
	 *
	 * Added {@Link TextContentCreator} responsible for the text effects
	 *
	 * @param textContent text from the parsing function (pattern)
	 */
	private String rawText = "";

	private transient TextContentCreator textContentCreator;

	private transient OneLineText oneLineStringDisplay;

	private transient OneLineText oneLineStringEdit;

	public SingleLineText()
	{
		super();
		createContentStructure();
	}

	public SingleLineText(Converter converter)
	{
		super(converter);
		createContentStructure();
	}

	protected SingleLineText(SingleLineText lineText) throws CloneNotSupportedException
	{
		super(lineText);
		oneLineStringDisplay = lineText.getOneLineStringDisplay().clone();
		oneLineStringEdit = lineText.getOneLineStringEdit().clone();
		textContentCreator = lineText.getTextContentCreator().clone();
	}

	private void createContentStructure()
	{
		setPadding(5, 10);
		createContextTextContent();
	}

	private void createContextTextContent()
	{
		oneLineStringDisplay = new OneLineText();
		oneLineStringEdit = new OneLineText();
		textContentCreator = new TextContentCreator(getTextColor());
	}

	@Override
	public void reconstruction(Converter converter)
	{
		super.reconstruction(converter);
		createContextTextContent();
		initializeTextEffects();
	}

	@Override
	public SingleLineText clone()
	{
		return (SingleLineText) super.clone();
	}

	@Override
	protected SingleLineText copy() throws CloneNotSupportedException
	{
		return new SingleLineText(this);
	}

	/**
	 * @see EditableText#setText(String)
	 * @code key method in text extracting process for Class
	 */
	@Override
	final public void setText(String text)
	{
		// [1] parse original text
		textContentCreator.acquireTextProperties(text);

		// [2] load primary color into JLabel foreground
		setTextColor(getTextContentCreator().getTextColor());

		// [3] initialize Class fields due to its purpose described above
		rawText = textContentCreator.getTextContent();
		oneLineStringDisplay = converter.toLineString(rawText);
		oneLineStringEdit = new OneLineText(text);

		// [4] initialize JLabel with Colored Text
		setParsedText();

		// [5] restore apply
		rawText = text;
		notifyAboutChange();
	}

	/**
	 * @see LineText#setLabelText(String)
	 */
	private void setParsedText(MultiColoredText coloredText)
	{
		if (coloredText == null || coloredText.isEmpty())
		{
			setLabelText(toDisplay());
		}
		else
		{
			setLabelText(toDisplay(), coloredText);
		}
	}

	/**
	 * @see LineText#setLabelText(String)
	 */
	private void setParsedText()
	{
		setParsedText(getTextContentCreator().getTextColors());
	}

	/**
	 * @see EditableText# Display()
	 */
	@Override
	final public String toDisplay()
	{
		return getOneLineStringDisplay().toDisplay();
	}

	/**
	 * @see EditableText#toEdit()
	 */
	@Override
	final public String toEdit()
	{
		return getOneLineStringEdit().toEdit();
	}

	/**
	 * @see Object#toString()
	 */
	@Override
	final public String toString()
	{
		return getOneLineStringEdit().toString();
	}

	private void initializeTextEffects()
	{
		setPadding(0, 10);
		setText(rawText);
	}

	/**
	 * @return one line text to Display
	 */
	private OneLineText getOneLineStringDisplay()
	{
		if (null == oneLineStringDisplay)
		{
			oneLineStringDisplay = new OneLineText();
			initializeTextEffects();
		}
		return oneLineStringDisplay;
	}

	/**
	 * @return one line text to Edit
	 */
	private OneLineText getOneLineStringEdit()
	{
		if (null == oneLineStringEdit)
		{
			oneLineStringEdit = new OneLineText();
			initializeTextEffects();
		}
		return oneLineStringEdit;
	}

	/**
	 * @return text parsing class TextContentCreator
	 */
	private TextContentCreator getTextContentCreator()
	{
		if (null == textContentCreator)
		{
			textContentCreator = new TextContentCreator(getTextColor());
		}
		return textContentCreator;
	}

}
