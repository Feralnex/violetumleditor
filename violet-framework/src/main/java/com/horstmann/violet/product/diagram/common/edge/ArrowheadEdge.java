package com.horstmann.violet.product.diagram.common.edge;

import com.horstmann.violet.product.diagram.property.ArrowheadChoiceList;
import com.horstmann.violet.product.diagram.property.choiceList.ChoiceList;
import com.horstmann.violet.product.diagram.abstracts.edge.arrowhead.Arrowhead;

import java.awt.*;

/**
 * Represents an edge with arrowheads
 */
public abstract class ArrowheadEdge extends LineEdge
{
    public ArrowheadEdge()
    {
        super();

        startArrowheadChoiceList = new ArrowheadChoiceList();
        endArrowheadChoiceList = new ArrowheadChoiceList();
        switchArrowheads = false;

        setStartArrowhead(ArrowheadChoiceList.NONE);
        setEndArrowhead(ArrowheadChoiceList.NONE);
    }

    protected ArrowheadEdge(ArrowheadEdge arrowheadEdge)
    {
        super(arrowheadEdge);
        this.startArrowheadChoiceList = arrowheadEdge.startArrowheadChoiceList.clone();
        this.endArrowheadChoiceList = arrowheadEdge.endArrowheadChoiceList.clone();
        this.selectedStartArrowhead = arrowheadEdge.startArrowheadChoiceList.getSelectedPos();
        this.selectedEndArrowhead = arrowheadEdge.endArrowheadChoiceList.getSelectedPos();
        this.switchArrowheads = arrowheadEdge.switchArrowheads;
    }

    @Override
    protected void beforeReconstruction()
    {
        super.beforeReconstruction();

        startArrowheadChoiceList = new ArrowheadChoiceList();
        endArrowheadChoiceList = new ArrowheadChoiceList();

        startArrowheadChoiceList.setSelectedIndex(selectedStartArrowhead);
        endArrowheadChoiceList.setSelectedIndex(selectedEndArrowhead);

        Color startArrowheadBackgroundColor = startArrowheadChoiceList.getSelectedValue().getFilledColor();
        Color endArrowheadBackgroundColor = endArrowheadChoiceList.getSelectedValue().getFilledColor();
        Color startArrowheadBorderColor = startArrowheadChoiceList.getSelectedValue().getBorderColor();
        Color endArrowheadBorderColor = endArrowheadChoiceList.getSelectedValue().getBorderColor();


        if(startArrowheadBackgroundColor != null) setArrowheadBackgroundColor(getStartArrowhead(), startArrowheadBackgroundColor);
        if(endArrowheadBackgroundColor != null) setArrowheadBackgroundColor(getEndArrowhead(), endArrowheadBackgroundColor);
        if(startArrowheadBorderColor != null) setArrowheadBorderColor(getStartArrowhead(), startArrowheadBorderColor);
        if(endArrowheadBorderColor != null) setArrowheadBorderColor(getEndArrowhead(), endArrowheadBorderColor);
    }

    @Override
    public void setBackgroundColor(Color bgColor) {
        super.setBackgroundColor(bgColor);
        setArrowheadBackgroundColor(getStartArrowhead(), bgColor);
        setArrowheadBackgroundColor(getEndArrowhead(), bgColor);
    }

    @Override
    public void setBorderColor(Color borderColor) {
        super.setBorderColor(borderColor);
        setArrowheadBorderColor(getStartArrowhead(), borderColor);
        setArrowheadBorderColor(getEndArrowhead(), borderColor);
        setBackgroundColor(borderColor);
    }

    private void setArrowheadBackgroundColor(Arrowhead arrowhead, Color bgColor) {
        if(arrowhead.getFilledColor() != null && !arrowhead.getFilledColor().equals(Color.WHITE) && !bgColor.equals(Color.WHITE)){
            arrowhead.setFilledColor(bgColor);
        }
    }


    private void setArrowheadBorderColor(Arrowhead arrowhead, Color borderColor) {
    	if(borderColor.equals(Color.WHITE)) {
    	    arrowhead.setBorderColor(borderColor);
            if(arrowhead.getFilledColor() != null && !arrowhead.getFilledColor().equals(Color.WHITE)) {
                arrowhead.setFilledColor(Color.LIGHT_GRAY);
            }
        } else {
            arrowhead.setBorderColor(borderColor);
        }
    }

    /**
     * Draws the edge.
     *
     * @param graphics the graphics context
     */
    @Override
    public void draw(Graphics2D graphics)
    {
        if (!switchArrowheads)
        {
            super.draw(graphics);
            getStartArrowhead().draw(graphics, contactPoints[1], contactPoints[0]);
            getEndArrowhead().draw(graphics, contactPoints[contactPoints.length - 2], contactPoints[contactPoints.length - 1]);
        }
        else
        {
            super.draw(graphics);
            getStartArrowhead().draw(graphics, contactPoints[contactPoints.length - 2], contactPoints[contactPoints.length - 1]);
            getEndArrowhead().draw(graphics, contactPoints[1], contactPoints[0]);
        }
    }

    public final void invertArrowHeads()
    {
        setSwitchArrowheads(!this.switchArrowheads);
    }

    public final ChoiceList getStartArrowheadChoiceList()
    {
        return startArrowheadChoiceList;
    }

    public final void setStartArrowheadChoiceList(ChoiceList startArrowheadChoiceList)
    {
        this.startArrowheadChoiceList = (ArrowheadChoiceList) startArrowheadChoiceList;
        this.selectedStartArrowhead = this.startArrowheadChoiceList.getSelectedPos();
    }

    public final ChoiceList getEndArrowheadChoiceList()
    {
        return endArrowheadChoiceList;
    }

    public final void setEndArrowheadChoiceList(ChoiceList endArrowheadChoiceList)
    {
        this.endArrowheadChoiceList = (ArrowheadChoiceList) endArrowheadChoiceList;
        this.selectedEndArrowhead = this.endArrowheadChoiceList.getSelectedPos();
    }

    public final Arrowhead getStartArrowhead()
    {
        return startArrowheadChoiceList.getSelectedValue();
    }

    protected final void setStartArrowhead(Arrowhead startArrowhead)
    {
        if (startArrowheadChoiceList.setSelectedValue(startArrowhead))
        {
            this.selectedStartArrowhead = startArrowheadChoiceList.getSelectedPos();
        }
    }

    public final Arrowhead getEndArrowhead()
    {
        return endArrowheadChoiceList.getSelectedValue();
    }

    protected final void setEndArrowhead(Arrowhead endArrowhead)
    {
        if (endArrowheadChoiceList.setSelectedValue(endArrowhead))
        {
            this.selectedEndArrowhead = endArrowheadChoiceList.getSelectedPos();
        }
    }

    public boolean isSwitchArrowheads()
    {
        return switchArrowheads;
    }

    public void setSwitchArrowheads(boolean switchArrowheads)
    {
        this.switchArrowheads = switchArrowheads;
    }

    private transient ArrowheadChoiceList startArrowheadChoiceList = new ArrowheadChoiceList();
    private transient ArrowheadChoiceList endArrowheadChoiceList = new ArrowheadChoiceList();

    private int selectedStartArrowhead;
    private int selectedEndArrowhead;
    private boolean switchArrowheads;
}
