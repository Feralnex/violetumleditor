package com.horstmann.violet.product.diagram.property.text;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
  @author MankindEnemy
  @date 06.01.2019
  */

public class PrepreparedTextColor
{
	public PrepreparedTextColor() {
		textColor = new HashMap<Integer, Color>();
		textColor.put(1, DEFAULT_COLOR);
		textColor.put(2, PASTEL_GREY);
		textColor.put(3, PASTEL_RED);
		textColor.put(4, PASTEL_RED_ORANGE);
		textColor.put(5, PASTEL_YELLOW_ORANGE);
		textColor.put(6, PASTEL_YELLOW);
		textColor.put(7, PASTEL_PEA_GREEN);
		textColor.put(8, PASTEL_YELLOW_GREEN);
		textColor.put(9, PASTEL_GREEN);
		textColor.put(10, PASTEL_GREEN_CYAN);
		textColor.put(11, PASTEL_CYAN);
		textColor.put(12, PASTEL_CYAN_BLUE);
		textColor.put(13, PASTEL_BLUE);
		textColor.put(14, PASTEL_BLUE_VIOLET);
		textColor.put(15, PASTEL_VIOLET);
		textColor.put(16, PASTEL_VIOLET_MAGENTA);
		textColor.put(17, PASTEL_MAGENTA);
		textColor.put(18, PASTEL_MAGENTA_RED);
	}

	/**
	 @return returns the selected color
	 */
	public Color getPrepreparedColor(Integer id){
		if(textColor.containsKey(id)){
			return this.textColor.get(id);
		} else {
			return null;
		}
	}

	// Source : http://www.tinygorilla.com/Easter_eggs/pallatehex.html
	// http://www.colorhexa.com/
	public static final Color DEFAULT_COLOR = Color.WHITE;
	public static final Color PASTEL_GREY = new Color(240, 240, 240);
	private static final Color PASTEL_RED = new Color(250, 189, 170);
	private static final Color PASTEL_RED_ORANGE = new Color(251, 205, 178);
	public static final Color PASTEL_YELLOW_ORANGE = new Color(254, 222, 188);
	private static final Color PASTEL_YELLOW = new Color(255, 251, 205);
	private static final Color PASTEL_PEA_GREEN = new Color(219, 235, 194);
	private static final Color PASTEL_YELLOW_GREEN = new Color(195, 227, 191);
	private static final Color PASTEL_GREEN = new Color(166, 217, 185);
	private static final Color PASTEL_GREEN_CYAN = new Color(178, 226, 223);
	private static final Color PASTEL_CYAN = new Color(182, 231, 250);
	private static final Color PASTEL_CYAN_BLUE = new Color(185, 207, 234);
	private static final Color PASTEL_BLUE = new Color(185, 194, 225);
	private static final Color PASTEL_BLUE_VIOLET = new Color(184, 180, 216);
	private static final Color PASTEL_VIOLET = new Color(200, 185, 217);
	private static final Color PASTEL_VIOLET_MAGENTA = new Color(217, 190, 219);
	private static final Color PASTEL_MAGENTA = new Color(249, 200, 222);
	private static final Color PASTEL_MAGENTA_RED = new Color(250, 199, 202);

	private Map<Integer, Color> textColor;

}
