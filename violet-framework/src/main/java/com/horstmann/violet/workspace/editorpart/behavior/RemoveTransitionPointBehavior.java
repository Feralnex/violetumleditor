package com.horstmann.violet.workspace.editorpart.behavior;

import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.SwingUtilities;

import com.horstmann.violet.product.diagram.abstracts.IGridSticker;
import com.horstmann.violet.product.diagram.abstracts.edge.IEdge;
import com.horstmann.violet.workspace.editorpart.IEditorPart;
import com.horstmann.violet.workspace.editorpart.IEditorPartSelectionHandler;
import com.horstmann.violet.workspace.sidebar.graphtools.IGraphToolsBar;

public class RemoveTransitionPointBehavior extends AbstractEditorPartBehavior {

    public RemoveTransitionPointBehavior(IEditorPart editorPart, IGraphToolsBar graphToolsBar) {
        this.editorPart = editorPart;
        this.selectionHandler = editorPart.getSelectionHandler();
    }

    @Override
    public void onMousePressed(MouseEvent event) {
        if (SwingUtilities.isMiddleMouseButton(event)) {
            double zoom = editorPart.getZoomFactor();
            IGridSticker gridSticker = editorPart.getGraph().getGridSticker();
            final Point2D mousePoint = new Point2D.Double(event.getX() / zoom, event.getY() / zoom);
            this.newTransitionPointLocation = mousePoint;
            this.newTransitionPointLocation = gridSticker.snap(this.newTransitionPointLocation);
            this.createNewTransitionPoints(mousePoint, this.getSelectedEdge().getTransitionPoints());
        }
    }

    @Override
    public void onMouseReleased(MouseEvent event) { }

    private IEdge getSelectedEdge() {
        if (this.selectedEdge == null) {
            if (this.selectionHandler.getSelectedEdges().size() == 1) {
                this.selectedEdge = this.selectionHandler.getSelectedEdges().get(0);
            }
        }
        return this.selectedEdge;
    }

    private void createNewTransitionPoints(Point2D mousePointToRemove, Point2D[] oldTransitionPoints) {
        Point2D transitionPointToRemove = this.findTransitionPointToRemove(mousePointToRemove, oldTransitionPoints);
        if (oldTransitionPoints.length > 0 && transitionPointToRemove != null) {
            List<Point2D> newTransitionPointsList = new ArrayList<Point2D>(Arrays.asList(oldTransitionPoints));
            System.out.println(newTransitionPointsList.size());
            newTransitionPointsList.removeIf(point -> point.getX() == transitionPointToRemove.getX()
                    && point.getY() == transitionPointToRemove.getY());
            System.out.println(newTransitionPointsList.size());
            getSelectedEdge()
                    .setTransitionPoints(newTransitionPointsList.toArray(new Point2D[newTransitionPointsList.size()]));
            this.selectedEdge = null;
        }
    }

    private Point2D findTransitionPointToRemove(Point2D mousePointToRemove, Point2D[] oldTransitionPoints) {
        Point2D transitionPointToRemove = null;
        double maxDifferenceX = 20;
        double maxDifferenceY = 20;
        double differenceX = 20;
        double differenceY = 20;

        for (int index = 0; index < oldTransitionPoints.length; index++) {
            differenceX = Math.abs(mousePointToRemove.getX() - oldTransitionPoints[index].getX());
            differenceY = Math.abs(mousePointToRemove.getY() - oldTransitionPoints[index].getY());
            if (differenceX <= maxDifferenceX && differenceY <= maxDifferenceY) {
                maxDifferenceX = differenceX;
                maxDifferenceY = differenceY;
                transitionPointToRemove = mousePointToRemove;
            }
        }
        return transitionPointToRemove;
    }

    private IEditorPartSelectionHandler selectionHandler;

    private IEditorPart editorPart;

    private IEdge selectedEdge = null;

    private Point2D newTransitionPointLocation = null;

}
