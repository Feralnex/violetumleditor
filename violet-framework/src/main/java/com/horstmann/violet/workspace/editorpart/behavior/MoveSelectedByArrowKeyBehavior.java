package com.horstmann.violet.workspace.editorpart.behavior;

import com.horstmann.violet.product.diagram.abstracts.IGridSticker;
import com.horstmann.violet.product.diagram.abstracts.edge.IEdge;
import com.horstmann.violet.product.diagram.abstracts.node.INode;
import com.horstmann.violet.workspace.editorpart.EditorPartSelectionHandler;
import com.horstmann.violet.workspace.editorpart.IEditorPart;
import com.horstmann.violet.workspace.editorpart.IEditorPartSelectionHandler;
import com.horstmann.violet.workspace.sidebar.graphtools.GraphTool;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.List;

/**
 * Allows to move the selected nodes with arrow keys
 */

public class MoveSelectedByArrowKeyBehavior extends AbstractEditorPartBehavior
{
    private IEditorPart editorPart;
    private IGridSticker gridSticker;
    private IEditorPartSelectionHandler selectionHandler;
    private List<INode> selectedNodes;
    private INode lastNode;
    private int moveDistance = 10;

    private double moveX;
    private double moveY;

    public MoveSelectedByArrowKeyBehavior(IEditorPart editorPart){
        this.editorPart = editorPart;
        this.gridSticker = this.editorPart.getGraph().getGridSticker();
        this.selectionHandler = this.editorPart.getSelectionHandler();
        this.selectedNodes = selectionHandler.getSelectedNodes();
        this.lastNode = selectionHandler.getLastSelectedNode();
    }

    private void onKeyPressed() {
        GraphTool selectedTool = this.selectionHandler.getSelectedTool();
        if (IEdge.class.isInstance(selectedTool.getNodeOrEdge())) {
            return;
        }

        this.lastNode = selectionHandler.getLastSelectedNode();

        Point2D currentNodeLocation = lastNode.getLocation();
        Point2D futureNodeLocation = new Point2D.Double(currentNodeLocation.getX() + moveX, currentNodeLocation.getY() + moveY);
        Point2D fixedFutureNodeLocation = gridSticker.snap(futureNodeLocation);

        if (fixedFutureNodeLocation.getX()<0||fixedFutureNodeLocation.getY()<0){
            return;
        }

        if (!currentNodeLocation.equals(fixedFutureNodeLocation)) {
            this.lastNode.setLocation(fixedFutureNodeLocation);
        }
        editorPart.getSwingComponent().invalidate();
        editorPart.getSwingComponent().repaint();
    }

    public void moveUp() {
        this.moveX = 0;
        this.moveY = -moveDistance;
        onKeyPressed();
    }

    public void moveDown() {
        this.moveX = 0;
        this.moveY = moveDistance;
        onKeyPressed();
    }

    public void moveLeft() {
        this.moveX = -moveDistance;
        this.moveY = 0;
        onKeyPressed();
    }

    public void moveRight() {
        this.moveX = moveDistance;
        this.moveY = 0;
        onKeyPressed();
    }

}
