package com.horstmann.violet.workspace.editorpart.behavior;

import com.horstmann.violet.product.diagram.abstracts.edge.IEdge;
import com.horstmann.violet.product.diagram.common.edge.ArrowheadEdge;
import com.horstmann.violet.workspace.editorpart.IEditorPart;
import com.horstmann.violet.workspace.editorpart.IEditorPartSelectionHandler;

import java.util.List;

public class InvertBehavior extends AbstractEditorPartBehavior {

    private IEditorPart editorPart;
    private IEditorPartSelectionHandler selectionHandler;
    public InvertBehavior(IEditorPart editorPart) {
        this.editorPart = editorPart;
        this.selectionHandler = editorPart.getSelectionHandler();
    }

    public void invertArrowheads()
    {

        List<IEdge> selectedEdges = selectionHandler.getSelectedEdges();
        if (!selectedEdges.isEmpty())
        {
            for (IEdge aSelectedEdge : selectedEdges)
            {
                ((ArrowheadEdge) aSelectedEdge).invertArrowHeads();
            }
        }

        editorPart.getSwingComponent().invalidate();
        editorPart.getSwingComponent().repaint();

    }
}
