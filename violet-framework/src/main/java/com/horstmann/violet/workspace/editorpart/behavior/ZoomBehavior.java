package com.horstmann.violet.workspace.editorpart.behavior;

import java.awt.event.KeyEvent;
import java.awt.event.MouseWheelEvent;

import com.horstmann.violet.framework.util.KeyModifierUtil;
import com.horstmann.violet.workspace.editorpart.IEditorPart;

/**
 *
 * class used to zoom in two ways:
 * - mouse wheel
 * - ctrl numpad+ and ctrl numpad- (graph tool bar need to be focused)
 *
 */

public class ZoomBehavior extends AbstractEditorPartBehavior
{

    private IEditorPart editorPart;

    public ZoomBehavior(IEditorPart editorPart)
    {
        this.editorPart = editorPart;
    }

    @Override
    public void onMouseWheelMoved(MouseWheelEvent event)
    {
        if (!KeyModifierUtil.isCtrl(event))
        {
            return;
        }
        int scroll = event.getUnitsToScroll();
        if (scroll < 0)
        {
            this.editorPart.changeZoom(1);
        }
        if (scroll > 0)
        {
            this.editorPart.changeZoom(-1);
        }
    }

    @Override
    public void onKeyPressed(KeyEvent e) {

        final KeyEvent keyEvent = (KeyEvent) e;

        if(keyEvent.isControlDown() && e.getKeyCode() == KeyEvent.VK_ADD) {

            this.editorPart.changeZoom(1);

        } else if (keyEvent.isControlDown() && e.getKeyCode() == KeyEvent.VK_SUBTRACT){

            this.editorPart.changeZoom(-1);
        }
    }

}
