package com.horstmann.violet.workspace.editorpart.behavior;

import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.CompoundEdit;

import com.horstmann.violet.product.diagram.abstracts.edge.IColorableEdge;
import com.horstmann.violet.product.diagram.abstracts.node.IColorableNode;
import com.horstmann.violet.workspace.sidebar.colortools.ColorChoice;

/**
 * Undo/Redo behavior triggered when node and edges are colorized
 *
 * @author Alexandre de Pellegrin
 *
 */
public class UndoRedoOnColorizeBehavior extends AbstractEditorPartBehavior
{

    /**
     * The global undo/redo behavior which contains all individual undo/redo behaviors
     */
    private UndoRedoCompoundBehavior compoundBehavior;

    private ColorChoice oldColorChoice;
    private ColorChoice newColorChoice;

    /**
     * Default constructor
     *
     * @param compoundBehavior
     */
    public UndoRedoOnColorizeBehavior(UndoRedoCompoundBehavior compoundBehavior)
    {
        this.compoundBehavior = compoundBehavior;
    }

    @Override
    public void beforeChangingColorOnElement(IColorableNode element)
    {
        reset();
        this.oldColorChoice = new ColorChoice(element.getBackgroundColor(), element.getBorderColor(), element.getTextColor());
    }

    @Override
    public void afterChangingColorOnElement(final IColorableNode element)
    {
        this.newColorChoice = new ColorChoice(element.getBackgroundColor(), element.getBorderColor(), element.getTextColor());
        this.compoundBehavior.startHistoryCapture();
        CompoundEdit currentCapturedEdit = this.compoundBehavior.getCurrentCapturedEdit();
        currentCapturedEdit.addEdit(new UndoableColorEdit(element, this.oldColorChoice, this.newColorChoice));
        this.compoundBehavior.stopHistoryCapture();
        reset();
    }

    @Override
    public void beforeChangingColorOnElement(IColorableEdge element) {
        reset();
        this.oldColorChoice = new ColorChoice(element.getBackgroundColor(), element.getBorderColor(), element.getTextColor());
    }

    @Override
    public void afterChangingColorOnElement(IColorableEdge element) {
        this.newColorChoice = new ColorChoice(element.getBackgroundColor(), element.getBorderColor(), element.getTextColor());
        this.compoundBehavior.startHistoryCapture();
        CompoundEdit currentCapturedEdit = this.compoundBehavior.getCurrentCapturedEdit();
        currentCapturedEdit.addEdit(new UndoableColorEdit(element, this.oldColorChoice, this.newColorChoice));
        this.compoundBehavior.stopHistoryCapture();
        reset();
    }

    private void reset()
    {
        this.oldColorChoice = null;
        this.newColorChoice = null;
    }

    private class UndoableColorEdit extends AbstractUndoableEdit
    {

        private IColorableNode elementNode;
        private IColorableEdge elementEdge;
        private ColorChoice oldColorChoice;
        private ColorChoice newColorChoice;

        public UndoableColorEdit(IColorableNode element, ColorChoice oldColorChoice, ColorChoice newColorChoice)
        {
            this.elementNode = element;
            this.oldColorChoice = oldColorChoice;
            this.newColorChoice = newColorChoice;
        }

        public UndoableColorEdit(IColorableEdge element, ColorChoice oldColorChoice, ColorChoice newColorChoice)
        {
            this.elementEdge = element;
            this.oldColorChoice = oldColorChoice;
            this.newColorChoice = newColorChoice;
        }

        @Override
        public void undo() throws CannotUndoException
        {
            if (elementNode != null) {
                this.elementNode.setBackgroundColor(this.oldColorChoice.getBackgroundColor());
                this.elementNode.setBorderColor(this.oldColorChoice.getBorderColor());
                this.elementNode.setTextColor(this.oldColorChoice.getTextColor());
            } else if (elementEdge != null) {
                this.elementEdge.setBackgroundColor(this.oldColorChoice.getBackgroundColor());
                this.elementEdge.setBorderColor(this.oldColorChoice.getBorderColor());
                this.elementEdge.setTextColor(this.oldColorChoice.getTextColor());
            }
        }

        @Override
        public void redo() throws CannotRedoException
        {
            if (elementNode != null) {
                this.elementNode.setBackgroundColor(this.newColorChoice.getBackgroundColor());
                this.elementNode.setBorderColor(this.newColorChoice.getBorderColor());
                this.elementNode.setTextColor(this.newColorChoice.getTextColor());
            } else if (elementEdge != null) {
                this.elementEdge.setBackgroundColor(this.newColorChoice.getBackgroundColor());
                this.elementEdge.setBorderColor(this.newColorChoice.getBorderColor());
                this.elementEdge.setTextColor(this.newColorChoice.getTextColor());
            }
        }

    }

}
