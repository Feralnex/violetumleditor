package com.horstmann.violet.workspace.editorpart.behavior;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import com.horstmann.violet.workspace.sidebar.graphtools.IGraphToolsBar;

public class ResetGraphToolBarBehavior extends AbstractEditorPartBehavior
{

    public ResetGraphToolBarBehavior(IGraphToolsBar graphToolsBar)
    {
    	this.graphToolsBar = graphToolsBar;
    }
    
    
    @Override
    public void onMouseClicked(MouseEvent event) {
    	boolean isButton3Clicked = (event.getButton() == MouseEvent.BUTTON3);
        if (event.getClickCount() == 1 && isButton3Clicked)
        {
            this.graphToolsBar.reset();
        }
    }

    @Override
    public void onKeyPressed(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.VK_ESCAPE)
        {
            this.graphToolsBar.reset();
        }
    }

    private IGraphToolsBar graphToolsBar;

  
}
